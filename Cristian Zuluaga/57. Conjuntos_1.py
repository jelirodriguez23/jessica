import os
os.system("cls")

BAL = set()
FUT = set()
AMB = set()

continuar = True

while (continuar):
    print("POR FAVOR DIGITE LOS DATOS DEL ALUMNO")

    nom = input("Digite Nombre ")
    dep = input("Digite Deporte (FUTBOL/BALONCESTO/AMBOS)")

    if (dep.upper() == "FUTBOL"):
        FUT.add(nom)
    elif (dep.upper() == "BALONCESTO"):
        BAL.add(nom)
    elif (dep.upper() == "AMBOS"):
        FUT.add(nom)
        BAL.add(nom)
        AMB.add(nom)

    c = input("DESEA REGISTRAR OTRO ALUMNO? (S/N)")
    continuar = (c.upper()=="S")

# Solo jueguen futbol
print("Lista de participantes Futbol",FUT)

# Solo jueguen baloncesto
print("Lista de participantes Baloncesto",BAL)


# Participen en las dos secciones al tiempo
print("Inscritos en futbol y baloncesto",BAL & FUT)

# Jueguen futbol o baloncesto
print("Inscritos en futbol o baloncesto",BAL | FUT)