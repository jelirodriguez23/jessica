import json

data = {}
data["clientes"] = []

data["clientes"].append({
    "Nombre":"Theodoric",
    "Apellido":"Rivers",
    "Edad":36,
    "Puntaje":1.11})

data["clientes"].append({
    "Nombre":input("Digite nombre: "),
    "Apellido":input("Digite apellido: "),
    "Edad":int(input("Digite edad: ")),
    "Puntaje":float(input("Digite puntaje: "))})

with open("data.json","w") as file:
    json.dump(data,file,indent=4)