import random
import os
os.system("cls")

cont = 0
matriz = []
n=int(input("Dimensión de la matriz Cuadrada? "))

matriz = [[0 for c in range(n)] for f in range(n)]

for fila in matriz:
    print(fila)

h=int(input("Digite la posicion horizontal "))
v=int(input("Digite la posicion vertical "))

v = v -1
h = h -1

matriz[v][h] = "X"

for fila in matriz:
    print(fila)

# Analisis de posiciones
if (-1 < (v+2) < 8):
    if (-1 < (h+1) < 8):
        matriz[v+2][h+1]=1
    if (-1 < (h-1) < 8):
        matriz[v+2][h-1]=1

if (-1 < (v-2) < 8):
    if (-1 < (h+1) < 8):
        matriz[v-2][h+1]=1
    if (-1 < (h-1) < 8):
        matriz[v-2][h-1]=1

if (-1 < (h+2) < 8):
    if (-1 < (v+1) < 8):
        matriz[v+1][h+2]=1
    if (-1 < (v-1) < 8):
        matriz[v-1][h+2]=1

if (-1 < (h-2) < 8):
    if (-1 < (v+1) < 8):
        matriz[v+1][h-2]=1
    if (-1 < (v-1) < 8):
        matriz[v-1][h-2]=1

print("Las posiciones posibles son las siguientes")

for fila in matriz:
    print(fila)  
