import os
os.system("cls")
import random

MatrizA = []
MatrizB = []
MatrizC = []
f1 = int(input("Numero de filas 1ra matriz "))
c1 = int(input("Numero de Columnas 1ra matriz "))
f2 = int(input("Numero de filas 2da matriz "))
c2 = int(input("Numero de Columnas 2da matriz "))

for filaA in range (f1):
    MatrizA.append ([])
    for colA in range (c1):
        MatrizA[filaA].append(random.randint(1,20))

for filaB in range (f2):
    MatrizB.append ([])
    for colB in range (c2):
        MatrizB[filaB].append(random.randint(1,20))

print ("Primera matriz")
for filasA in MatrizA:
    print(filasA)

print ("Segunda matriz")
for filasB in MatrizB:
    print(filasB)

# Validación de la operación
if (c1 != f2):
    print ("Las matrices no son compatibles para una multiplicación!!!")

for filaC in range (f2):
    MatrizC.append ([])
    for colC in range (c2):
        MatrizC[filaC].append(0)

# Multiplicación de matrices
for i in range (f1):
    for j in range (c2):
        for k in range (f2):
            MatrizC[i][j] += MatrizA[i][k]*MatrizB[k][j]

print ("La matriz resultante es ")
for filasC in MatrizC:
    print(filasC)