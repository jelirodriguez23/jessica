import random
import os
os.system("cls")

matriz = []
n=int(input("Dimensión de la matriz Cuadrada?"))

# for fila in range (n):
#     matriz.append([])
#     for col in range (n):
#         matriz[fila].append(0)
# print(matriz)


matriz = [[0 for c in range(n)] for f in range(n)]

print(matriz)

for fila in matriz:
    print(fila)
print()
for i in range (n):
    print(matriz[i])

# Esta es una forma de asignación múltiple:
i, f, c = 0, 0, (n//2)

# Esta es otra forma de asignación múltiple:
i = f = 0
# la variable i obtiene los valores a llenar desde 1 hasta n^2

i = 0
f = 0
c = n//2

print()
print(i,f,c)
print()

while i < (n*n):
    i = i + 1
    matriz[f][c] = i
    if (f-1) == -1:
        if ((c+1)==n):
            f= f+1
        else:
            f = n-1
            c = c+1
    else:
        if ((c+1)==n):
            c = 0
            f = f-1
        else:
            f = f-1
            c = c+1

    if matriz[f][c] != 0:
        f = f+2
        c = c-1
    # Aquí se cierra el ciclo while
for fila in matriz:
    print(fila)

#1 Suma de filas
for fila in matriz:
    suma = 0
    for i in range (n):
        suma = suma + fila [i]
    print (fila, "=", suma)

#2 Suma de columnas
print("Suma de las columnas en orden ascendente")
for fila in matriz:
    for a in fila:
        suma = 0
        for b in range (n):
            suma = suma + fila[b]
    print (suma)

#3 Suma Diagonal Principal
print("Suma de la diagonal principal es")
suma = 0
for a in range (n):
    suma = suma + matriz[a][a]
print (suma)

#4 Suma Diagonal Secundaria
print("Suma de la diagonal secundaria es")
c = n-1
suma = 0
for d in range (n):
    suma = suma + matriz[d][c]
    c=c-1
print(suma)