
import os 
os.system("cls")

# MILISTA=list()
# mitupla=tuple()

# for i in range(1,11):
#     MILISTA.append(i)
# print(MILISTA)

meses=(1,2,3,4,5,6,7,8,9,10,11,12)

for elem in meses:
    print(elem)
print()

#Al igual que las listas, las tuplas también pueden ser anidadas.
tupla = 1, 2, ('a', 'b'), 3
print(tupla)       
print(tupla[2][0]) 

print()

#Y también es posible convertir una lista en tupla haciendo uso de al función tuple().
lista = [1, 2, 3]
tupla = tuple(lista)
print(type(tupla)) 
print(tupla)       
print()

#Y se puede también asignar el valor de una tupla con n elementos a n variables.
listas = (1, 2, 3)
x, y, z = listas
print(x, y, z)
print()

#El método count() cuenta el número de veces que el objeto pasado como parámetro se ha encontrado en la lista.
l2 = [1, 1, 1, 3, 5]
print(l2.count(1))
print()

#El método index() busca el objeto que se le pasa como parámetro y devuelve el índice en el que se ha encontrado.
l3 = [7, 7, 7, 3, 5]
print(l3.index(5))
print()

#El método index() también acepta un segundo parámetro opcional, que indica a partir de que índice empezar a buscar el objeto.
l4= [7, 7, 7, 3, 5]
print(l4.index(7, 2))
print()

x="python"
print(type(x))
print(len(x))
print()

x2="python",
print(type(x2))
print(len(x2))