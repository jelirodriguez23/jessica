#Elabore un programa python que lea varios números y calcule la suma de las raíces cuadradas de numeros leídos la lectura termina cuando
#el número ingresado sea CERO. Hacer uso Try-Except para controlar los posibles errores de lectura 

import os
import math

os.system("cls")

suma_raices=0
lecturavalida=False
v=True
while not lecturavalida:
    try:
        while(v):
            numero = int(input("Ingresar el numero de la raíz: "))
            raiz_cuadrada = math.sqrt(numero)
            suma_raices=suma_raices+raiz_cuadrada
            if(numero==0):
                v=False
                lecturavalida=True
            print(suma_raices)
    except ZeroDivisionError:
        print("No se puede dividir por cero")
    except ValueError:
        print("Dato no valido")
    except IOError:
        print("No tiene acceso a la lectura del número")
    except:
        print("Error desconocido")

    else:
        print("El resultado de la suma de raíces es:",suma_raices)