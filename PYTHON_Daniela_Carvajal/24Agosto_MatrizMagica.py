import os
import random
os.system("cls")

suma=0
colum=[]
matriz2=[]
n=int(input("Dimensión de la matriz cuadrada?:"))
matriz=[[0 for c in range (n)] for f in range (n)]

print(matriz)
print()

for fila in matriz:
    print(fila)
print()

for i in range (n):
    print(matriz[i])

#La variable i obtiene los valores a llenar desde 1 hasta n^2
#La variable f "fila" comienza en cero para hacer el conte 0 ,1 ,2,3 
# C para empezar a llenar en la mitad de la matriz 

#Esto es una asignación multiple (ASIGNACIÓN TIPO TUPLAS)
i,f,c=0,0,n//2

print("\n",i,f,c)
print()

while i < (n*n):
    i=i+1
    matriz[f][c]=i
    if (f-1)== -1:
        if((c+1)==n):
            f=f+1
        else:
            f=n-1
            c=c+1
    else:
        if((c+1)==n):
            c=0
            f=f-1    
        else:
            f=f-1
            c=c+1

    if matriz[f][c]!=0:
        f=f+2
        c=c-1

        #Aquí se cierra el ciclo while 

for fila in matriz:
    print(fila)
print()


# for fila in matriz:
#     suma=0
#     for i in range (n):
#         suma=suma+fila[i]
#     print(fila,"=",suma)


for fila in matriz:
    print(fila,"=",sum(fila))
print()

# for f in range(n):
#     for e in range(n):
#         print(matriz[f][e])
#         print(matriz[f][0])
#         print(matriz[f][n-1])
#         print(matriz[f][f])

for c in range(n):
    d=matriz[c][n-1]
    colum.append(d)
print("La columna ultima es: ",colum)

for colum in matriz:
    suma2=0
    for i in range (n):
        suma2=suma2+colum[i]
    print(colum,"=",suma2)