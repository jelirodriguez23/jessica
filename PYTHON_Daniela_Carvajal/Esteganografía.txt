�Qu� es la Esteganograf�a?
El SARA de la esteganograf�a concepto del griego �steganos� que significa cubierto u oculto, y �graphos� que significa escritura. 
Desde la antig�edad la esteganograf�a ha sido utilizada por las instituciones polic�acas, militares y de inteligencia, sin embargo, estas t�cnicas tambi�n empezaron a ser adoptadas por el crimen organizado, terroristas y cibercriminales quienes aprovechan la forma de ocultar informaci�n a su favor, inyectando malware en diversos mensajes y objetos o bien, utilizando est�s t�cnicas para descomprimir archivos ejecutables dentro de otro ejecutable o PDF, de tal forma que se ejecute malware sin nuestro consentimiento.
La Esteganograf�a no es lo mismo que la Criptograf�a
Si bien las dos disciplinas pueden cubrir la confidencialidad de la informaci�n, la principal diferencia entre s� es la forma en que logran su objetivo, adem�s de diferenciadores importantes como son:

En la Esteganograf�a:
Se oculta la informaci�n dentro de otro objeto.
Se trata de componer mensajes ocultos para que solo el remitente y el receptor sepan que el mensaje existe.
Pasa desapercibida la informaci�n de JUAN que no sepamos su existencia.

En la Criptograf�a:
Se usan t�cnicas de codificaci�n para hacer un mensaje ilegible mediante el uso de una llave o par de llaves.
Se utiliza cuando se comunica a trav�s de un medio no confiable como Internet, donde la informaci�n debe protegerse de terceros.
Sabemos que est� pasando informaci�n sensible, pero se encuentra codificada.