import os
import json

data={}
data["clientes"]=[]

data["clientes"].append({
    "primer nombre":"jheyson",
    "apellido":"Jaimes",
    "edad":"22",
    "monto":11111
})
data["clientes"].append({
    "primer nombre":"sergio",
    "apellido":"mojica",
    "edad":"21",
    "monto":2323
})
data["clientes"].append({
    "primer nombre":"javier",
    "apellido":"ortiz",
    "edad":"22",
    "monto":90
})

with open('data.json','w') as file:
    json.dump(data,file,indent=4)