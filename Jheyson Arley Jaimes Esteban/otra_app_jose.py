from tkinter import ttk
from tkinter import *
import sqlite3

class Laboratory:

    dbName = "farmaciaDB.db"

    def __init__(self, window):
        self.wind = window
        self.wind.title("Products application")

        # creating a frame container
        frame = LabelFrame(self.wind, text="Register a new project")
        frame.grid(row=0, column=0, columnspan=2, pady=20)

        # Name input
        Label(frame, text="Name: ").grid(row=1, column=0)
        self.name = Entry(frame)
        self.name.grid(row=1, column=1)
        self.name.focus()

        # price Label
        Label(frame, text="price: ").grid(row=2, column=0)
        self.phone = Entry(frame)
        self.phone.grid(row=2, column=1)

        # Button Add product
        ttk.Button(frame, text="save product", command=self.add_product).grid(row=3, columnspan=2, sticky=W+E)

        # Table
        self.Tree = ttk.Treeview(height= 10,columns=("#1","#2","#3"))
       
        self.Tree.heading("#0", text="Cod", anchor=CENTER)
        self.Tree.heading("#1", text="nom", anchor=CENTER)
        self.Tree.heading("#2", text="Tel", anchor=CENTER)
        self.Tree.heading("#3", text="Adress", anchor=CENTER)

        self.Tree.grid(row=4, column=0)

        
        self.get_labs()
    
    # Definir los query y parametros
    def run_query(self, query, parameters=()):
        with sqlite3.connect(self.dbName) as conn:
            cursor = conn.cursor()
            result = cursor.execute(query,parameters)
            conn.commit()
        return result
        
    # Ejecutar
    def get_labs(self):
        # cleaning  table
        records = self.Tree.get_children()
        for element in records:
            self.Tree.delete(element)
        # query data
        query = 'SELECT * FROM LABORATORIO ORDER BY nom_lab DESC'
        dbRows = self.run_query(query)
        print(dbRows)       
        # filling data
        for row in dbRows:
            print(row)
            self.Tree.insert('', 0, text=row[0],value=(row[1], row[2], row[3]))

    def validation(self):
        return len(self.name.get()) !=0 and len(self.phone.get()) !=0
    
    def add_product(self):
        if self.validation():
            print(self.name.get())
            print(self.phone.get())
            query = 'INSERT INTO LABORATORIO VALUES (NULL,\"'+self.name.get()+'\", \"'+self.phone.get()+'\", NULL)'
            self.run_query(query)
            print('Data saved')
            self.get_labs()

        else:
            print('Name and phone are required')
       


if __name__=="__main__":
    window=Tk()
    application = Laboratory(window)
    window.mainloop()