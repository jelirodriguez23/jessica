import os #llama a la libreria Operate System
from math import * #importamos todos los metodos de la libreria math

os.system("cls") #Llamamos la funcion cls: clear screen

for i in range(2): #se inicia el ciclo for
    r=int(input("ingrese el radio del circulo:")) #se lee de pantalla el valor del radio y se vuelve entero
    a=pi*pow(r,2) #calcula el area del circlo
    print("el area es:" , a) # se imprime en pantalla "el area es:" y el valor del area
    print("el area es {:.3f}".format(a)) # se imprime en pantalla "el area es:" y el valor del area con un formato de tres decimales despues del punto

    print("el area es:",round(a,3)) # se imprime en pantalla "el area es:" y el valor del area redondeado a tres decimales
    print(a) # se imprime en pantalla el valor de a

    print(round(pi,4),round(pi,50)) #se imprime en pantalla el valor de pi redondeado a 4 decimales y el valor de pi redondeado a 50 decimales
