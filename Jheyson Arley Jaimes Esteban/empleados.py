import os

os.system("cls")

empleados=list()

def order(dat,llave="ID"):
    for i in range(len(dat)-1):
        for j in range(i+1,len(dat)):
            if dat[i][llave] > dat[j][llave]:
                tem=dat[i]
                dat[i]=dat[j]
                dat[j]=tem
    return dat    
        

seguir=True
esp={"ID":0,"nombre":0,"edad":0,"AFP":0,"EPS":0,"ARL":0}
print("\n   Bienvenido al centro de manejo de empleados\n")
while seguir:
    print("\n Menu principal: \n 1. Agregar empleados. \n 2. Borrar empleado. \n 3. Listar empleados. \n 0. Salir")
    resp=int(input("Ingrese la opcion"))
    if resp==1:
        mas=True
        while(mas):
            print("Ingrese Los datos de empleado\n")
            empleado=dict()
            
            empleado["ID"]=int(input("Ingrese el ID : "))
            empleado["nombre"]=input("Ingrese el nombre: ")
            empleado["edad"]=int(input("Ingrese la edad: "))
            empleado["AFP"]=input("Ingrese la AFP: ")
            empleado["EPS"]=input("Ingrese la EPS: ")
            empleado["ARL"]=input("Ingrese la ARL: ")
            for i in esp:
                if len(str(empleado[i]))>esp[i]:
                    esp[i]=len(str(empleado[i]))

            empleados.append(empleado)

            r=input("\ndesea agregar otro empleado (S:si N:no):")
            if r.upper()=="S" :
                mas=True
            else:
                mas=False
    elif(resp==2):
    
        print("\nSelecciones el criterio de busqueda: \n 1. ID \n 2. nombre \n 3. edad \n 4. AFP \n 5. EPS \n 6. ARS " )
        crt=input("Ingrese su respuesta:")
        diccops={"1":"ID","2":"nombre","3":"edad","4":"AFP","5":"EPS","6":"ARL"}
        crt=diccops[crt]
        ind=input("Ingrese el "+crt+" del usuario")
        n=0
        for i in empleados:
            if(i[crt]==ind):
                empleados.remove(n)
                n+=1
                print("Se a eliminado correctamente")

    elif(resp==3):
        print("\nSelecciones el criterio de ornedamiento: \n 1. ID \n 2. nombre \n 3. edad \n 4. AFP \n 5. EPS \n 6. ARS " )
        crt=input("Ingrese su respuesta:")
        diccops={"1":"ID","2":"nombre","3":"edad","4":"AFP","5":"EPS","6":"ARL"}
        crt=diccops[crt]
        # n=1
        # for i in empleados:
        #     print("Empleado ",n)
        #     for j in i:
        #         print(j.ljust(9),":",i[j])
        #     n+=1
        #     print()

        # print()
        
        empleados=order(empleados,crt)
        print("\n Ordenados por ",crt)
        n=1
        for i in empleados:
            txt=""
            txt+="Empleado "+str(n)+" : "
            for j in i:
                txt+=str(i[j]).ljust(esp[j]+2)+" "
            n+=1
            print(txt)