import os
import json

def leer(txt="dato",entero=False):
    mal=True
    while mal:
        try:
            if(entero):
                n=int(input("Ingrese "+txt+": "))
            else:
                n=input("Ingrese "+txt+": ")
        except Exception as error:
            print(" el dato presta un error:",error," Intente de nuevo")
        else:
            mal=False
    return n

def buscar(cedula,js):
    for i in range(len(js["clientes"])):
        if js["clientes"][i]["cedula"]==cedula:
            pos=i
            return i
    return -1

def ingresa(js):
    resp="s"
    while resp.upper()=="S":
        cc=leer("la cedula")
        if buscar(cc,js)>=0:
            print("El usuario ya se encuentra registrado")
        else:
            js["clientes"].append({
                "cedula": cc,
                "primer nombre":leer("el nombre"),
                "apellido":leer("el apellido"),
                "edad":leer("la edad",True),
                "monto":leer("el monto",True)
            })
        resp=input("Desea continuar registrando (N=No S=Si):")
        print("\n")

def edit(cedula,js):
    n=buscar(cedula,js)
    if(n>0):
        cliente=js["clientes"][n]
        print("Losa datos actuales son:")
        for i in cliente.keys():
            print(i.ljust(10)," = ",cliente[i])
        print("ingrese nuevos datos:")
        for i in cliente.keys():
            js["clientes"][n][i]=leer(i,(type(js["clientes"][n][i])==int))
    else:
        print("El cliente no existe.")

def elim(cedula,js):
    n=buscar(cedula,js)
    if(n>0):
        js["clientes"].pop(n)
        print("el cliente fue eliminado")
    else:
        print("El cliente no existe.")

def most(js):
    for i in js["clientes"]:
        print("_"*35)
        for j in i.keys():
            print("|",j.ljust(15),"|",str(i[j]).ljust(15),"|")
        print("_"*35)


data={}
data["clientes"]=[]
op=0
while op!="6":
    
    os.system("cls")
    print("Menu principal \n 1. Ingresar cliente \n 2. Buscar cliente \n 3. editar cliente \n 4. Eliminar cliente \n 5. Mostrar clientes \n 6. Salir ")

    op=input("\nSeleccione una opcion: ")
    if op=="1":
        ingresa(data)
    elif op=="2":
        cc=input("Ingrese la cedula a buscar")
        print("el usuario es el cliente #",buscar(cc,data))
    elif op=="3":
        cc=input("Ingrese la cedula a editar")
        edit(cc,data)
    elif op=="4":
        cc=input("Ingrese la cedula a eliminar")
        elim(cc,data)
    elif op=="5":
        most(data)
    input("\npresione cualquier tecla para continuar...")


with open('clientes.json','w') as file:
        json.dump(data,file,indent=4)