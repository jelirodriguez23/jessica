import os #llama a la libreria Operate System
import math #llama a la libreria Math

os.system("cls") #Llamamos la funcion cls: clear screen
print("Ingrese el radio de la base del cono:") #imprimir en consola instrucciones
r=int(input()) #leer de consola la variable r que contiene el radio del la base del circulo
print("Ingrese la altura del cono:")  #imprimir en consola instrucciones
h=int(input()) #leer de consola la variable h que contiene la altura del cono

a= math.pi*(r**2) #calcula el area de la base del cono
print("El area de la base es:",a) #imprimir en pantalla el texto y el valor del area de la base del cono
v=a*h/3 #calcula el volumen del cono
print("El volumen del cono es: ",v) #imprimir en pantalla el texto y el valor del volumen del cono



