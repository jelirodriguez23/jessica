from django.db import models
from django.db.models.fields.related import ForeignKey

# Create your models here.

class Empleado(models.Model):
    id= models.CharField(max_length=50, primary_key= True)
    nombre= models.CharField(max_length=50, null=True)
    apellido = models.CharField(max_length=50, null=True)
    contrasena = models.CharField(max_length=50, null= True)
    telefono = models.CharField(max_length=50, null=True)

class Habitacion(models.Model):
    Numero_Habitacion = models.CharField(max_length=50, primary_key= True)
    Cupo_maximo= models.CharField(max_length=50, null = True)
    Tipo_Habitacion= models.CharField(max_length=50, null=True)
    Estado_Habitacion = models.CharField(max_length=50, null=True)

class Huesped(models.Model):
    ID_Huesped = models.CharField(max_length=50, primary_key= True,default="1")
    Nombre= models.CharField(max_length=50, null= True)
    Apellido= models.CharField(max_length=50, null=True)
    Correo = models.CharField(max_length=50, null=True)
    Telefono = models.CharField(max_length=50, null = True)
    Tipo_Tarjeta = models.CharField(max_length=50, null = True)
    Numero_Tarjeta = models.CharField(max_length=50, null = True)
    Fecha_Expedicion  = models.CharField(max_length=50, null = True)

class Reserva(models.Model):
    Cod_Reserva = models.CharField(max_length=50, primary_key= True)
    Fecha_Ingreso = models.CharField(max_length=50)
    Fecha_Salida = models.CharField(max_length=50, null=True)
    Cantidad_Ninos = models.CharField(max_length=50, null=True)
    Cantidad_Adultos = models.CharField(max_length=50, null=True)
    Estado_Reserva = models.CharField(max_length=50, null=True)
    Numero_Habitacion =  models.ForeignKey(Habitacion,on_delete=models.CASCADE)
    ID_Huesped = models.ForeignKey(Huesped,on_delete=models.CASCADE)
    ID_Empleado = models.ForeignKey(Empleado,on_delete=models.CASCADE)

class Factura(models.Model):
    ID_Factura=models.CharField(max_length=50, primary_key= True)
    Valor= models.CharField(max_length=50, null=True)
    Fecha = models.CharField(max_length=50, null=True)
    Hora = models.CharField(max_length=50, null=True)
    Descripcion_Servicio = models.CharField(max_length=50, null=True)
    Cod_Reserva = models.ForeignKey(Reserva,on_delete=models.CASCADE)