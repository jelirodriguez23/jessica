from django.contrib.auth.forms import AuthenticationForm
from django.forms.forms import Form
from django.shortcuts import redirect, render

from Modulos.applab.models import Habitacion
from Modulos.applab.models import Huesped
from Modulos.applab.models import Reserva
from Modulos.applab.models import Factura
from Modulos.applab.models import Empleado
from django.contrib.auth import login,logout,authenticate
from django.contrib import messages
from Modulos.applab.formularios import ReservaFrm

# Create your views here.

def logg(request):
    
    if request.method == "POST":
        form=AuthenticationForm(request,data=request.POST)
        if form.is_valid():
            usuario=form.cleaned_data.get('username')
            contraseña=form.cleaned_data.get('password')
            user=authenticate(username=usuario,password=contraseña)
            if user is not None:
                login(request,user)
                messages.error(request,f"Estas logeado como{usuario}")
                return redirect("index")
            else:
                messages.error(request,"Usiuario o contraseña invalidos")
        else:
            messages.error(request,"Usiuario o contraseña invalidos")

    form= AuthenticationForm()
    return render(request,"log.html",{'form':form})

def loggout(request):
    logout(request)
    return redirect('login')


def index(request):
    habits=Habitacion.objects.all()
    hues=Huesped.objects.all()
    res=Reserva.objects.all()
    fac=Factura.objects.all()
    em=Empleado.objects.all()

    print(habits)
    return(render(request,"index.html",{"misEmpleados":em,"misFacturas":fac,"misHabitaciones":habits,"misHuespedes":hues,"misReservas":res}))

def editar(request,id):

    return()

def eliminar_Reserva(request,id):
    res=Reserva.objects.get(Cod_Reserva=id)
    res.delete()
    return redirect('index')

def editar_Reserva(request,id):
    res=Reserva.objects.get(Cod_Reserva=id)
    if request.method == 'GET':
        form=ReservaFrm(instance=res)
    else:
        form=ReservaFrm(request.POST,instance=res)
        if form.is_valid():
            form.save()
            return redirect('index')
    contexto={
        'formulario':form
    }

    return render(request,'editar.html',contexto)