from django import forms
from django.forms import fields

from Modulos.applab.models import Reserva


class ReservaFrm(forms.ModelForm):
    class Meta:
        model=Reserva
        fields="__all__"