# Generated by Django 3.2.8 on 2021-10-22 01:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('applab', '0003_rename_facutura_factura'),
    ]

    operations = [
        migrations.RenameField(
            model_name='empleado',
            old_name='contraseña',
            new_name='contrasena',
        ),
        migrations.RenameField(
            model_name='huesped',
            old_name='ID_Husped',
            new_name='ID_Huesped',
        ),
    ]
