import os
import pathlib

def leer(imprimir=False):
    archivo=open("texto.txt","r",encoding="utf8")
    conten=archivo.read()
    if imprimir:
        print("\n"+conten)
        print("\nLa ubicacion del archivo es: ",pathlib.Path('texto.txt').absolute())
    archivo.seek(0)
    return archivo,conten

def contarPalabras():
    archivo,cont=leer()
    print("El texto contiene ",len(cont.split()),"palabras")

def repeticionPalabra(palabra):
    archivo,cont=leer()
    pal=cont.split()
    n=pal.count(palabra)
    print("La palabra ",palabra," aparece ",n," veces")

def remplazar(pal,npal):
    archivo,cont=leer()
    txt=""
    for i in archivo.readlines():
        for j in i.split():
            if j==pal:
                j=npal
            txt+=j+" "
        txt+="\n"
    archivo.close()
    archivo = open('texto.txt','w', encoding="utf8")
    archivo.write(txt)
    print("se a remplazado correctamente")

archivo=leer()[0]
seg=True
while seg:
    
    os.system("cls")
    print("\t Menu principal \n 1. leer todo el archivo\n 2. Cantidad de palabras del texto\n 3. Cantidad de veces de una palabra\n 4. Reemplazar una palabra en todo el texto\n 5. Finalizar")
    op=input("Elije una opcion: ")
    if op=="1":
        archivo=leer(True)[0]
    elif op=="2":
        contarPalabras()
    elif op=="3":
        palabra=input("Ingrese la palabra a contar\n")
        repeticionPalabra(palabra)
    elif op=="4":
        pal=input("Palabra a replazar: ")
        npal=input("Palabra de replazo: ")
        remplazar(pal,npal)
    elif op=="5":
        seg=False
    else:
        print("la opcion no es valida")
    archivo.seek(0)
    input("presione una telca para continuar...")
archivo.close()