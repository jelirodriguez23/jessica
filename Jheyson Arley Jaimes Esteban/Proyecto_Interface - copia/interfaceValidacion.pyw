import re
import sqlite3
import os
from sqlite3.dbapi2 import Error
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from typing import Dict
from tkcalendar import Calendar 
import Buscar
import Grafico_Hotel
from PIL import ImageTk, Image

conect=False
try:
    con=sqlite3.connect("hotel.db")
    cursor=con.cursor()
except:
    print("no se pudo conectar")
else:
    print("conectado")
    conect=True

imagenes2=dict()


LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)
logueado=False

def f_fechas(f1="00/00/00",f2="00/00/00",function="-"):
    if(function=="-"):
        dats1=f1.split("/")
        dats2=f2.split("/")
        dif=(int(dats2[2])-int(dats1[2]))*365
        dif+=(int(dats2[1])-int(dats1[1]))*30
        dif+=(int(dats2[0])-int(dats1[0]))
        print(dats1)
        print(dats2)
        print(dif)
        return dif


def calend(n):
    
    calnd =Tk()
    cal=Calendar(calnd, selectmode = 'day', 
               year = 2020, month = 5, 
               day = 22)
    cal.pack()
    if n==0:
        aceptar=Button(calnd,command=lambda :[ckin.config(text=cal.get_date()),calnd.destroy(),b_ckin.config(state=NORMAL)],text="Acepto",activebackground="green")
        aceptar.pack()

    else:
        aceptar=Button(calnd,command=lambda :[ckout.config(text=cal.get_date()) if f_fechas(ckin.cget("text"),cal.get_date())>0 else messagebox.showerror("Fecha de salida anterior a fecha de ingreso"),
                          calnd.destroy(),b_ckout.config(state=NORMAL)],text="Acepto")
        aceptar.pack()
    calnd.protocol("WM_DELETE_WINDOW", lambda:[calnd.destroy(),b_ckin.config(state=NORMAL),b_ckout.config(state=NORMAL)])

def logg():

    root=Tk()
    root.title(" --> LOGIN <--")
    root.config(bg=RGRAY,bd=0,highlightthickness=0)
    root.geometry("400x180")
    root.resizable(height=False, width=False)

    root.columnconfigure(0,weight=0)
    root.columnconfigure(1,weight=1)

    etiq1=Label(root,text="LOGGIN EMPLEADO", bg=RGRAY,fg="white")
    etiq1.grid(padx=3,pady=15,row=0,column=0,columnspan=2)

    etiq1=Label(root,text="USUARIO:", bg=RGRAY,fg="white")
    etiq1.grid(padx=7,pady=3,row=1,column=0)

    etiq2=Label(root,text="CONTRASEÑA:", bg=RGRAY,fg="white")
    etiq2.grid(padx=7,pady=3,row=2,column=0)

    e1=Entry(root)
    e1.grid(row=1,column=1)

    e2=Entry(root,show="*")
    e2.grid(row=2,column=1)

    b1=Button(root, text="Ingresar",command=lambda :validar(e1.get(),e2.get(),root),width="20" ,bg = "goldenrod1",fg = "black", activebackground="green",relief=RAISED)
    b1.grid(pady=20,row=4,column=0,columnspan=2)

    root.mainloop()

def npersonas():
    b_nper.config(state=DISABLED)
    niños=int(nn.cget("text"))
    adultos=int(na.cget("text"))
    p1=Frame()
    p1.config(width="150",height="300",bd=2,relief=RIDGE)
    p1.place(x=5,y=200)
    la=Label(p1,text="Adultos").grid(row=0,column=0,columnspan=3)
    lna=Label(p1,text=str(adultos),relief=SUNKEN)
    lna.grid(row=1,column=1)
    bnas=Button(p1,text="+",command=lambda :[lna.config(text=str(int(lna.cget("text"))+1)),nper.config(text=str(int(nper.cget("text"))+1))],activebackground="green").grid(row=1,column=2)
    bnar=Button(p1,text="-",command=lambda :[lna.config(text=str(int(lna.cget("text"))-1)) if ( int(lna.cget("text"))>0 ) else False, nper.config(text=str(int(lnn.cget("text"))+int(lna.cget("text"))))],activebackground="green").grid(row=1,column=0)
    ln=Label(p1,text="niños").grid(row=2,column=0,columnspan=3)
    lnn=Label(p1,text=str(niños),relief=SUNKEN)
    lnn.grid(row=3,column=1)
    bnns=Button(p1,text="+",command=lambda :[lnn.config(text=str(int(lnn.cget("text"))+1)),nper.config(text=str(int(nper.cget("text"))+1))],activebackground="green").grid(row=3,column=2)
    bnnr=Button(p1,text="-",command=lambda :[lnn.config(text=str(int(lnn.cget("text"))-1)) if ( int(lnn.cget("text"))>0 ) else False, nper.config(text=str(int(lnn.cget("text"))+int(lna.cget("text"))))],activebackground="green").grid(row=3,column=0)
    ba=Button(p1,text="Aceptar",command=lambda :[nn.config(text=lnn.cget("text")),na.config(text=lna.cget("text")),p1.destroy(),b_nper.config(state=NORMAL)],activebackground="green").grid(row=4,column=0)
    bc=Button(p1,text="Cancelar",command=lambda :[nper.config(text=str(niños+adultos)),p1.destroy(),b_nper.config(state=NORMAL)],activebackground="red").grid(row=4,column=2)
   
def validar(d1,d2,root):
    nombre=d1
    contraseña=d2
    acceso=False
    for us in users:
        if (nombre in us) and (contraseña in us):
            root.destroy()           
            acceso=True
            global logueado
            logueado=True
    if not(acceso):
        messagebox.showerror(title="INGRESO",message="acceso denegado")

def reservar(dat):
    print(dat)
    window = Tk()
    aplication = Grafico_Hotel.Huesped(window,dat)
    window.mainloop()

def dispo():
    
    ventana = Tk()
    ventana.title("HABITACIONES")
    ventana.geometry("600x600+100+100")
    ventana.config(bg= DGRAY)

    for i in range(5):
        ventana.columnconfigure(i,weight=1)
    ventana.rowconfigure(0,weight=1)
    ventana.rowconfigure(1,weight=1)
    ventana.rowconfigure(2,weight=1)

    caja1=PanedWindow(ventana,bg="light gray",relief=SOLID)
    caja1.grid(padx=10,pady=5,row=0,column=0,columnspan=5,sticky=NSEW)
    caja1.columnconfigure(0,weight=1)
    caja1.columnconfigure(1,weight=1)
    caja1.columnconfigure(2,weight=1)
    caja1.rowconfigure(0,weight=1)
    caja1.rowconfigure(1,weight=1)
    cont1=Label(caja1,bg="light gray")
    cont1.grid(row=0,column=0)
    l1=Label(cont1, text= "FILTRAR HABITACIONES POR:", bg="light gray",font=("Verdana",10))
    l1.grid(row=0,column=0)
    lista_desplegable = ttk.Combobox(cont1,width=30,state="readonly")
    lista_desplegable.grid(row=1,column=0)
    lista_desplegable.set("SELECCIONE")

    cont2=Label(caja1)
    cont2.grid(row=0,column=1)
    C1 = Label(cont2,text = "checkin", width=10)
    C1.grid(row=0,column=0)
    Label(cont2,text=ckin.cget("text")).grid(row=1,column=0)
    C2 = Label(cont2,text = "checkout", width=10)
    C2.grid(row=0,column=1)
    Label(cont2,text=ckout.cget("text")).grid(row=1,column=1)
    C3 = Label(cont2,text = "habitaciones", width=10)
    C3.grid(row=2,column=0)
    Label(cont2,text=nhab.cget("text")).grid(row=3,column=0)
    C4 = Label(cont2,text = "personas", width=10)
    C4.grid(row=2,column=1)
    Label(cont2,text=nper.cget("text")).grid(row=3,column=1)
    b1 = Button(caja1, text="MODIFICAR", command=rec,bg = "goldenrod1",activebackground="light green", relief=RAISED)
    b1.grid(padx=10,row=0,column=2)
    lista_desplegable["values"] = ["MAYOR PRECIO","MENOR PRECIO","CANTIDAD DE PERSONAS"]

    caja2=PanedWindow(ventana,bg="light gray",relief=SOLID)
    caja2.grid(padx=10,pady=5,row=1,column=0,columnspan=5,sticky=NSEW)
    caja2.columnconfigure(0,weight=1)
    caja2.columnconfigure(1,weight=1)
    caja2.columnconfigure(2,weight=1)
    caja2.rowconfigure(0,weight=1)
    caja2.rowconfigure(1,weight=1)

    
    v3 = Frame(caja2)
    v3.config(width=145, height= 180, bg= "navajo white",bd=2, relief=RIDGE)
    v3.grid(row=0,column=0,rowspan=3)

    lista_habitacion1 = Listbox(v3, borderwidth=0, bg= "navajo white")
    lista_habitacion1.pack(pady=15)
    conshabit=cursor.execute("select Numero_Habitacion from Habitacion")
    opciones = conshabit.fetchall()
    habitaciones = opciones#aca iria el nombre de la  habitacion al oprimir el boton

    for item in habitaciones:
        lista_habitacion1.insert(END, item[0])

    caracteristicas = Listbox(caja2, activestyle=NONE, borderwidth=0, bg= "navajo white")
    caracteristicas.grid(row=0,column=1,rowspan=2)

    C5 = Label(caja2,text = "PRECIO X NOCHE", width=15, height= 5,borderwidth=2,relief="solid", bd=2,bg="light gray")
    C5.grid(pady=5,row=2,column=1)
    value=""
    b2 = Button(ventana, command=lambda :[reservar( str((lista_habitacion1.get(ACTIVE)))),ventana.destroy()], text="RESERVAR",bg = "goldenrod1",borderwidth=2, activebackground="light green", relief="raised")
    b2.grid(padx=5,pady=5,row=2,column=4)

    
    def CurSelet(evt):
        global value 
        value=str((lista_habitacion1.get(ACTIVE)))
        print (value)
        caracteristicas.delete(0,END)
        
        consa=cursor.execute("select Servicios from Habitacion where Numero_Habitacion = \""+value+"\"")
        a=consa.fetchall()[0][0]

        for item in a.split(";"):
            caracteristicas.insert(END, item)

    lista_habitacion1.bind('<<ListboxSelect>>',CurSelet)
    
def reser():

        v5= Tk()
        v5.title('Busquedad de reservas')
        v5.iconbitmap("")
        v5.geometry('400x180')
        v5.config( bg=RGRAY, relief='raised', bd=0,highlightthickness=0)
        v5.resizable(0, 0) 
        aplication = Buscar.Busquedad_Reserva(v5)
        v5.mainloop()

def rec():
    ind=Tk()
    ind.title(" --> INDEX <--")
    ind.config(bg=DGRAY)
    ind.geometry("600x600")
    ind.resizable(height=False, width=False)
    cons=cursor.execute("select Numero_Habitacion,imagen from Habitacion")
    for i in cons.fetchall():
        ima=ImageTk.PhotoImage(Image.open(i[1]))
        imagenes2[i[0]]=ima

    tit=PanedWindow(ind,relief=RIDGE,bg="light gray")
    tit.columnconfigure(0,weight=1)
    tit.columnconfigure(1,weight=1)
    
    img_logo=ImageTk.PhotoImage(Image.open("logo.png"))
    Logo=Label(tit,image=img_logo, width=113,height=102,bg="light gray")
    Logo.grid(padx=3, pady=3, row=0,column=0,rowspan=4)
    Label(tit,bg="light gray",text="HOTEL LA LUNA ").grid(row=1,column=1)
    Label(tit,bg="light gray",text="El mejor lugar para descansar ").grid(row=2,column=1)
    Label(tit,bg="light gray").grid(row=3,column=3,pady=1,padx=50)

    nper=Label(ind,text="0",relief=SUNKEN)
    na=Label(ind,text="0",relief=SUNKEN)
    nn=Label(ind,text="0",relief=SUNKEN)
    b_nper=Button(ind,command=lambda :[npersonas()],text="+",bg = "goldenrod1",activebackground="green")
    nhab=Label(ind,text="0",relief=SUNKEN)
    b_nhabs=Button(ind,command=lambda :[nhab.config(text=str(int(nhab.cget("text"))+1))],text="+",bg = "goldenrod1",activebackground="green")
    b_nhabr=Button(ind,command=lambda :[nhab.config(text=str(int(nhab.cget("text"))-1)) if (int(nhab.cget("text")) > 0) else False],text="-",bg = "goldenrod1",activebackground="green")
    ckin=Label(ind,text="00/00/00",relief=SUNKEN)
    b_ckin=Button(ind,command=lambda n=0:[b_ckin.config(state=DISABLED),calend(n)],bg = "goldenrod1",text="\U0001F4C6")
    ckout=Label(ind,text="00/00/00",relief=SUNKEN)
    b_ckout=Button(ind,command=lambda n=1:[b_ckout.config(state=DISABLED),calend(n)],bg = "goldenrod1",text="\U0001F4C6")

    b_bdisp=Button(ind,command=lambda :[dispo(),ind.destroy()],text="Disponibilidad",bg = "light green",activebackground="green") 
    b_breser=Button(ind,command=lambda :[reser(),ind.destroy()],text="Buscar",bg = "light blue",activebackground="green") 
    #p_info=PanedWindow()

    imgs=dict()
    n=0
    for i in os.listdir("imgs"):
        imgs[n]=ImageTk.PhotoImage(Image.open("imgs\\"+i))
        n+=1
    print(imgs)
    n=0
    f_fondoimg=Label(ind,bg="light gray")
    f_img=Label(f_fondoimg, image=imgs[0],text=0, width=350,height=250,bg="light blue")
    b_next=Button(f_fondoimg,command= lambda :[f_img.config(image=imgs[int((f_img.cget("text"))+1)%len(imgs)],text=(int(f_img.cget("text"))+1)%len(imgs)) ],bg="goldenrod1",text=">")
    b_prev=Button(f_fondoimg,command= lambda :[f_img.config(image=imgs[int((f_img.cget("text"))-1)%len(imgs)],text=(int(f_img.cget("text"))-1)%len(imgs)) ],bg="goldenrod1",text="<")

    
    for i in range(10):
        ind.columnconfigure(i,weight=1)
    for i in range(8,11):
        ind.rowconfigure(i,weight=1)

    tit.grid(pady=5,row=1,column=1,columnspan=11,sticky=NSEW)
    Label(ind,bg=DGRAY).grid(row=2,column=12,pady=3,padx=7)
    Label(ind,text="# Personas",bg=DGRAY,fg="white").grid(pady=2, row=3,column=1,columnspan=2,sticky=NSEW)
    nper.grid(row=4,column=1,sticky=NSEW)
    b_nper.grid(row=4,column=2)
    Label(ind,text="# Habitaciones",bg=DGRAY,fg="white").grid(pady=2, row=3,column=3,columnspan=3,sticky=NSEW)
    b_nhabr.grid(row=4,column=3)
    nhab.grid(row=4,column=4,sticky=NSEW)
    b_nhabs.grid(row=4,column=5)
    Label(ind,text="Fecha ingreso",bg=DGRAY,fg="white").grid(pady=2, row=3,column=6,columnspan=2,sticky=NSEW)
    ckin.grid(row=4,column=6,sticky=NSEW)
    b_ckin.grid(row=4,column=7)
    Label(ind,text="Fecha salida",bg=DGRAY,fg="white").grid(pady=2, row=3,column=8,columnspan=2,sticky=NSEW)
    ckout.grid(row=4,column=8,sticky=NSEW)
    b_ckout.grid(row=4,column=9)
    b_bdisp.grid(padx=5, row=3,column=11)
    b_breser.grid(row=4,column=11)
    Label(ind,bg=DGRAY).grid(row=5,column=12,pady=7,padx=7)
    f_fondoimg.grid(row=6,column=1,columnspan=11,rowspan=4,sticky=NSEW,pady=5)
    f_fondoimg.columnconfigure(1,weight=1)
    f_fondoimg.rowconfigure(0,weight=1)
    f_img.grid(row=0,column=1,sticky=NSEW)
    b_prev.grid(row=0,column=0,sticky=NS)
    b_next.grid(row=0,column=2,sticky=NS)

    #p_info.grid(padx=20,pady=12,row=6,column=0,rowspan=4,columnspan=11,sticky=(N, S, E, W))
    ind.mainloop()

if conect:
    user=cursor.execute("Select * from Empleado")
    users=user.fetchall()
    print(users)
    
 


logg()
print(logueado)
if logueado:
    print("log")
    ind=Tk()
    ind.title(" --> INDEX <--")
    ind.config(bg=DGRAY)
    ind.geometry("600x600")
    ind.resizable(height=False, width=False)

    cons=cursor.execute("select Numero_Habitacion,imagen from Habitacion")
    for i in cons.fetchall():
        ima=ImageTk.PhotoImage(Image.open(i[1]))
        imagenes2[i[0]]=ima

    tit=PanedWindow(ind,relief=RIDGE,bg="light gray")
    tit.columnconfigure(0,weight=1)
    tit.columnconfigure(1,weight=1)
    
    img_logo=ImageTk.PhotoImage(Image.open("logo.png"))
    Logo=Label(tit,image=img_logo, width=113,height=102,bg="light gray")
    Logo.grid(padx=3, pady=3, row=0,column=0,rowspan=4)
    Label(tit,bg="light gray",text="HOTEL LA LUNA ").grid(row=1,column=1)
    Label(tit,bg="light gray",text="El mejor lugar para descansar ").grid(row=2,column=1)
    Label(tit,bg="light gray").grid(row=3,column=3,pady=1,padx=50)

    nper=Label(ind,text="0",relief=SUNKEN)
    na=Label(ind,text="0",relief=SUNKEN)
    nn=Label(ind,text="0",relief=SUNKEN)
    b_nper=Button(ind,command=lambda :[npersonas()],text="+",bg = "goldenrod1",activebackground="green")
    nhab=Label(ind,text="0",relief=SUNKEN)
    b_nhabs=Button(ind,command=lambda :[nhab.config(text=str(int(nhab.cget("text"))+1))],text="+",bg = "goldenrod1",activebackground="green")
    b_nhabr=Button(ind,command=lambda :[nhab.config(text=str(int(nhab.cget("text"))-1)) if (int(nhab.cget("text")) > 0) else False],text="-",bg = "goldenrod1",activebackground="green")
    ckin=Label(ind,text="00/00/00",relief=SUNKEN)
    b_ckin=Button(ind,command=lambda n=0:[b_ckin.config(state=DISABLED),calend(n)],bg = "goldenrod1",text="\U0001F4C6")
    ckout=Label(ind,text="00/00/00",relief=SUNKEN)
    b_ckout=Button(ind,command=lambda n=1:[b_ckout.config(state=DISABLED),calend(n)],bg = "goldenrod1",text="\U0001F4C6")

    b_bdisp=Button(ind,command=lambda :[dispo(),ind.destroy()],text="Disponibilidad",bg = "light green",activebackground="green") 
    b_breser=Button(ind,command=lambda :[reser(),ind.destroy()],text="Buscar",bg = "light blue",activebackground="green") 
    #p_info=PanedWindow()

    imgs=dict()
    n=0
    for i in os.listdir("imgs"):
        imgs[n]=ImageTk.PhotoImage(Image.open("imgs\\"+i))
        n+=1
    print(imgs)
    n=0
    f_fondoimg=Label(ind,bg="light gray")
    f_img=Label(f_fondoimg, image=imgs[0],text=0, width=350,height=250,bg="light blue")
    b_next=Button(f_fondoimg,command= lambda :[f_img.config(image=imgs[int((f_img.cget("text"))+1)%len(imgs)],text=(int(f_img.cget("text"))+1)%len(imgs)) ],bg="goldenrod1",text=">")
    b_prev=Button(f_fondoimg,command= lambda :[f_img.config(image=imgs[int((f_img.cget("text"))-1)%len(imgs)],text=(int(f_img.cget("text"))-1)%len(imgs)) ],bg="goldenrod1",text="<")

    
    for i in range(10):
        ind.columnconfigure(i,weight=1)
    for i in range(8,11):
        ind.rowconfigure(i,weight=1)

    tit.grid(pady=5,row=1,column=1,columnspan=11,sticky=NSEW)
    Label(ind,bg=DGRAY).grid(row=2,column=12,pady=3,padx=7)
    Label(ind,text="# Personas",bg=DGRAY,fg="white").grid(pady=2, row=3,column=1,columnspan=2,sticky=NSEW)
    nper.grid(row=4,column=1,sticky=NSEW)
    b_nper.grid(row=4,column=2)
    Label(ind,text="# Habitaciones",bg=DGRAY,fg="white").grid(pady=2, row=3,column=3,columnspan=3,sticky=NSEW)
    b_nhabr.grid(row=4,column=3)
    nhab.grid(row=4,column=4,sticky=NSEW)
    b_nhabs.grid(row=4,column=5)
    Label(ind,text="Fecha ingreso",bg=DGRAY,fg="white").grid(pady=2, row=3,column=6,columnspan=2,sticky=NSEW)
    ckin.grid(row=4,column=6,sticky=NSEW)
    b_ckin.grid(row=4,column=7)
    Label(ind,text="Fecha salida",bg=DGRAY,fg="white").grid(pady=2, row=3,column=8,columnspan=2,sticky=NSEW)
    ckout.grid(row=4,column=8,sticky=NSEW)
    b_ckout.grid(row=4,column=9)
    b_bdisp.grid(padx=5, row=3,column=11)
    b_breser.grid(row=4,column=11)
    Label(ind,bg=DGRAY).grid(row=5,column=12,pady=7,padx=7)
    f_fondoimg.grid(row=6,column=1,columnspan=11,rowspan=4,sticky=NSEW,pady=5)
    f_fondoimg.columnconfigure(1,weight=1)
    f_fondoimg.rowconfigure(0,weight=1)
    f_img.grid(row=0,column=1,sticky=NSEW)
    b_prev.grid(row=0,column=0,sticky=NS)
    b_next.grid(row=0,column=2,sticky=NS)

    #p_info.grid(padx=20,pady=12,row=6,column=0,rowspan=4,columnspan=11,sticky=(N, S, E, W))
    ind.mainloop()

con.close()