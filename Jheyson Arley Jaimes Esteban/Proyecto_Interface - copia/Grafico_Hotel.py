from os import name, system
system('cls')
import random

from tkinter import ttk
from tkinter import *
import sqlite3
from tkcalendar import Calendar
from tkinter import messagebox

LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)

class Huesped:
   
    db_name = 'Hotel.db'

    def __init__(self, n_habitacion="",empl={"id":"0","nom":"","cont":""},datos={"ckin":"00/00/00","ckout":"00/00/00","na":"0","nn":"0","nh":"0"}):
        self.datos=datos
        self.empleado=empl
        self.habit=n_habitacion
        print("num="+n_habitacion)
        self.wind = Tk()
        self.wind.title('Información Huesped')
        self.wind.configure(bg=DGRAY)
        wh=int((self.wind.winfo_screenheight()-400)/2)
        ww=int((self.wind.winfo_screenwidth()-560)/2)
        self.wind.geometry("800x400+"+str(ww)+"+"+str(wh))
        self.wind.iconbitmap('moon.ico')

        #Información del Huesped

        frame = LabelFrame(self.wind, text = 'Información Principal',bg=DGRAY,fg="white")      
        frame.grid(row = 5, column = 17, columnspan = 20, pady = 15, padx = 15)

        Label(frame, text = 'ID del Huesped: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 7, column = 19)
        self.ID_Huesped = Entry(frame)
        self.ID_Huesped.focus()
        self.ID_Huesped.grid(padx=2,pady=2,row = 9, column = 19)

        Label(frame, text = 'Nombre: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 19)
        self.Nombre = Entry(frame)
        self.Nombre.grid(padx=2,pady=2,row = 13, column = 19)

        Label(frame, text = 'Apellido: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 21)
        self.Apellido = Entry(frame)
        self.Apellido.grid(padx=2,pady=2,row = 13, column = 21)

        Label(frame, text = 'Correo: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 19)
        self.Correo = Entry(frame)
        self.Correo.grid(padx=2,pady=2,row = 17, column = 19)

        Label(frame, text = 'Número telefónico: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 21)
        self.Telefono = Entry(frame)
        self.Telefono.grid(padx=2,pady=2,row = 17, column = 21)

        #Información Pago

        frame1 = LabelFrame(self.wind, text = 'Información Pago' ,bg=DGRAY,fg="white")      
        frame1.grid(row = 23, column = 17, columnspan = 20, pady = 15, padx = 15)

        self.Tipo_Tarjeta = ttk.Combobox(frame1, width=20, state="readonly")
        self.Tipo_Tarjeta.grid(padx=5,pady=3,row = 25, column = 17)
        self.Tipo_Tarjeta.set("Tipo de tarjeta")
        opciones = ["Débito", "Crédito"]
        self.Tipo_Tarjeta["values"]  = opciones

        Label(frame1, text = 'Número de tarjeta: ',bg=DGRAY,fg="white").grid(row = 27, column = 17)
        self.Numero_Tarjeta=Entry(frame1)
        self.Numero_Tarjeta.grid(padx=5,pady=3,row = 28, column = 17,columnspan=2)

        #Información tarjeta de Pago

        Label(frame1, text = 'Fecha de vencimiento: ',bg=DGRAY,fg="white").grid(row = 27, column = 21)
        contf=PanedWindow(frame1,bg=DGRAY)
        contf.grid(row = 28, column = 21)
        contf.columnconfigure(1,weight=0)
        self.Fecha = Label(contf, text="00/00/00")
        self.Fecha.grid(padx=2,pady=2, row = 0, column = 0)
        self.Fecha_Vencimiento = Button(contf,command=lambda n=0:[self.calend(self,n),
        self.Button.config(state=DISABLED)], text="\U0001F4C6",bg = "goldenrod1")
        self.Fecha_Vencimiento.grid(padx=2,pady=2,row = 0, column = 1)
    

        #Botón de confirmar

        self.Button = Button(self.wind, text = 'Confirmar reserva', bg= "goldenrod1", command = self.code)
        self.Button.grid(pady=5,row = 30, column = 14, columnspan = 5)

        #Botón de cancelar

        self.Button = Button(self.wind, text = 'Cancelar', bg= "goldenrod1", command = self.code)
        self.Button.grid(pady=5,row = 30, column = 16, columnspan = 5)

        #Botón de consultar ID existente

        self.Button = Button(frame, text = 'Consultar ID', bg= "goldenrod1", command =lambda :self.valid_ID(self.ID_Huesped.get()))
        self.Button.grid(padx=2,pady=2,row = 9, column = 21)

        #Mensage de confirmación

        self.message = Label(text = '',bg=DGRAY, fg = 'green')
        self.message.grid(row = 32, column = 14, columnspan = 5)

        #Tabla contenido de Habitación (Información obtenida Programa WACO)

        frame2 = LabelFrame(self.wind, text = 'Descripción habitación',bg=DGRAY,fg="white")      
        frame2.grid(row = 5, column = 5, columnspan = 2, rowspan=39, pady = 15, padx = 15,sticky=NSEW)

        self.tree = ttk.Treeview(frame2, height = 10, columns = 2)
        self.tree.grid(row = 0, column = 0, padx= 15, pady= 15,sticky=NSEW)
        
        #Los datos que se hañadan acá se hacen con el llamado de la función
        # def = get_data y las variables que haya seleccionado WACO

        self.get_data(n_habitacion)

    def run_query(self, query, parameters = ()):
        with sqlite3.connect(self.db_name) as conn:
            cursor = conn.cursor()
            result = cursor.execute(query, parameters)
            conn.commit()
        return result
    
    def get_data(self,Hab):

        # Para limpiar, no creo que deba usarse
        # records = self.tree.get_children()
        # for elements in records:
        #     self.tree.delete(elements)
            
        query= 'SELECT * FROM Habitacion WHERE Numero_Habitacion = \"'+Hab+"\"" #Los datos necesarios no sólo están en la tabla de Habitacion, también los que se hayan añadido a Reserva
        db_rows = self.run_query(query)
        for row in db_rows:
                                   
            self.tree.insert('', 0, text = 'Servicios de habitacion', 
            value= row[5]) 
            self.tree.insert('', 0, text = 'Precio por noche', 
            value= row[4]) 
            self.tree.insert('', 0, text = 'Estado de habitación', 
            value= row[3])
            self.tree.insert('', 0, text = 'Tipo de habitación', 
            value= row[2]) 
            self.tree.insert('', 0, text = 'Cupo Maximo', 
            value= row[1]) 
            self.tree.insert('', 0, text = 'Número de habitación', 
            value= row[0]) 
            
        #Aquí el text y el value debería ser lo que seleccionó WACO en la app de él

        # text = 'Número de habitación: ', value = row[0]
        # text = 'Cupo máximo: ', value = row[]
        # text = 'Tipo habitación: ', ...
        # text = 'Fecha Ingreso: ',  ...
        # text = 'Fecha salida: ', ...
        # text = 'Cantidad de niños: ', 
        # text = 'Cantidad de adultos: ', 
        # text = 'Número de habitación: ', 
        # text = 'Valor: ', 

    def add_data_ID(self, ID_Huesped):
        ID=self.run_query("select * from Huesped where ID_Huesped = \""+ID_Huesped+"\"")
        IDH=ID.fetchall()
        self.Nombre.delete(0, END)
        self.Apellido.delete(0, END)
        self.Correo.delete(0, END)
        self.Telefono.delete(0, END)
        self.Numero_Tarjeta.delete(0, END)
        self.Fecha.config(text="00/00/00")

        if len(IDH)>0:
            IDH=IDH[0]
            print(ID_Huesped,IDH)

            self.Nombre.insert(0,IDH[1])
            self.Apellido.insert(0,IDH[2])
            self.Correo.insert(0,IDH[3])
            self.Telefono.insert(0,IDH[1])
            if IDH[5]=="Crédito":
                n=1
            else:
                n=0
            self.Tipo_Tarjeta.current(n)
            self.Numero_Tarjeta.insert(0,IDH[6])
            self.Fecha.config(text=IDH[7])
        

    def valid_ID(self, ID_Huesped):
        print(ID_Huesped)
        print("Select * from Huesped where ID_Huesped =\""+ID_Huesped+"\"")
        cons=cursor.execute("Select * from Huesped where ID_Huesped =\""+ID_Huesped+"\"")
      
        huesped=cons.fetchall()
        if(len(huesped)>0):
            self.add_data_ID(ID_Huesped)        
        else:
            messagebox.showerror(title="CONSULTA",message="El usuario no existe en la base de datos")


    def code(self): 
        cons=self.run_query("select * from Huesped where ID_Huesped=\""+self.ID_Huesped.get()+"\"") 
        if(len(cons.fetchall())==0):
            cc=self.ID_Huesped.get()
            nom=self.Nombre.get()
            ap=self.Apellido.get()
            corr=self.Correo.get()
            tel=self.Telefono.get()
            tipt=self.Tipo_Tarjeta.get()
            nt=self.Numero_Tarjeta.get()
            fe=self.Fecha.cget("text")

            query= 'INSERT INTO Huesped VALUES("'+cc+'","'+nom+'","'+ap+'","'+corr+'","'+tel+'","'+tipt+'","'+nt+'","'+fe+'")'
            print(query) 
            self.run_query(query)

        emp=self.empleado["id"]
        cc=self.ID_Huesped.get()
        fi=self.datos["ckin"]
        fs=self.datos["ckout"]
        nn=self.datos["nn"]
        na=self.datos["na"]
        st="Activo"
        
        query= 'INSERT INTO Reserva VALUES(Null,"'+fi+'","'+fs+'","'+nn+'","'+na+'","'+st+'","'+self.habit+'","'+cc+'","'+emp+'")' 
        db_rows = self.run_query(query)

        query="Slect * from Habitacion where Numero_Habitacion = \""+self.habit+"\""
        hab=self.run_query(query)[0]
        d=self.f_fechas(fi,fs)
        p=d*int(hab[4])
        query= 'INSERT INTO Factura VALUES(Null,"'+str(p)+'","000","1","asdasdas","001")' 
        db_rows = self.run_query(query)
        
    def validation(self):
        return len(self.ID_Huesped.get()) != 0 and len(self.Numero_Tarjeta.get()) != 0
    
    def add_data(self):
        if self.validation():
            query = 'INSERT INTO Huesped VALUES(?, ?, ?, ?, ?, ?, ?, ?)' #Agregar la cantidad de datos necesarios para llenar la tabla
            parameters = (self.ID_Huesped.get(), self.Nombre.get(), self.Apellido.get(),
            self.Correo.get(), self.Telefono.get(), self.Tipo_Tarjeta.get(), 
            self.Numero_Tarjeta.get(), self.Fecha.cget('text'))
            self.run_query(query, parameters)
            self.message['text'] = 'La reseva ha sido exitosa'
            self.ID_Huesped.delete(0, END)
            self.Nombre.delete(0, END)
            self.Apellido.delete(0, END)
            self.Correo.delete(0, END)
            self.Telefono.delete(0, END)
            self.Tipo_Tarjeta.delete(0, END) 
            self.Numero_Tarjeta.delete(0, END)
            # self.Fecha.delete(0, END)

        else:
            self.message['text'] = 'Todos los datos son requeridos'
        
        # self.get_Huesped()

    def calend(self,N,n):
        
        calnd =Tk()
        cal=Calendar(calnd, selectmode = 'day', 
                year = 2020, month = 5, 
                day = 22)
        cal.pack()
        
        aceptar=Button(calnd,command=lambda :[self.Fecha.config(text=cal.get_date()),
        calnd.destroy(),self.Fecha_Vencimiento.config(state=NORMAL)],bg = "goldenrod1",text="Acepto",
        activebackground="green")
        aceptar.pack()

    def f_fechas(self,f1="00/00/00",f2="00/00/00",function="-"):
        if(function=="-"):
            dats1=f1.split("/")
            dats2=f2.split("/")
            dif=(int(dats2[2])-int(dats1[2]))*365
            dif+=(int(dats2[1])-int(dats1[1]))*30
            dif+=(int(dats2[0])-int(dats1[0]))
            print(dats1)
            print(dats2)
            print(dif)
        return dif



conect=False
try:
    con=sqlite3.connect("Hotel.db")
    cursor=con.cursor()
except:
    print("no se pudo conectar")
else:
    print("conectado")
    conect=True

if conect:
    user=cursor.execute("Select * from Huesped")
    users=user.fetchall()
    print(users)
    


