from os import system
system('cls')
import random

from tkinter import ttk
from tkinter import *
import sqlite3
from tkcalendar import Calendar

LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)

class Huesped:
   
    db_name = 'Hotel.db'

    def __init__(self, window):

        self.wind = window
        self.wind.title('Información Huesped')
        self.wind.configure(bg=DGRAY)
        self.wind.iconbitmap('moon.ico')

        #Información del Huesped

        frame = LabelFrame(self.wind, text = 'Información Principal',bg=DGRAY,fg="white")      
        frame.grid(row = 5, column = 17, columnspan = 2, pady = 15, padx = 15)

        Label(frame, text = 'ID del Huesped: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 7, column = 19)
        self.ID_Huesped = Entry(frame)
        self.ID_Huesped.focus()
        self.ID_Huesped.grid(padx=2,pady=2,row = 9, column = 19)

        Label(frame, text = 'Nombre: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 19)
        self.Nombre = Entry(frame)
        self.Nombre.grid(padx=2,pady=2,row = 13, column = 19)

        Label(frame, text = 'Apellido: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 21)
        self.Apellido = Entry(frame)
        self.Apellido.grid(padx=2,pady=2,row = 13, column = 21)

        Label(frame, text = 'Correo: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 19)
        self.Correo = Entry(frame)
        self.Correo.grid(padx=2,pady=2,row = 17, column = 19)

        Label(frame, text = 'Número telefónico: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 21)
        self.Telefono = Entry(frame)
        self.Telefono.grid(padx=2,pady=2,row = 17, column = 21)

        #Información Pago

        frame1 = LabelFrame(self.wind, text = 'Información Pago' ,bg=DGRAY,fg="white")      
        frame1.grid(row = 23, column = 17, columnspan = 20, pady = 15, padx = 15)

        self.Tipo_Tarjeta = ttk.Combobox(frame1, width=20, state="readonly")
        self.Tipo_Tarjeta.grid(row = 25, column = 17)
        self.Tipo_Tarjeta.set("Tipo de tarjeta")
        opciones = ["Débito", "Crédito"]
        self.Tipo_Tarjeta["values"]  = opciones

        Label(frame1, text = 'Número de tarjeta: ',bg=DGRAY,fg="white").grid(row = 27, column = 17)
        self.Numero_Tarjeta=Entry(frame1)
        self.Numero_Tarjeta.grid(padx=5,pady=3,row = 28, column = 17,columnspan=2)

        #Información tarjeta de Pago

        Label(frame1, text = 'Fecha de vencimiento: ',bg=DGRAY,fg="white").grid(row = 27, column = 21)
        contf=PanedWindow(frame1,bg=DGRAY)
        contf.grid(row = 28, column = 21)
        contf.columnconfigure(1,weight=0)
        self.Fecha = Label(contf, text="00/00/00")
        self.Fecha.grid(padx=2,pady=2, row = 0, column = 0)
        self.Fecha_Vencimiento = Button(contf,command=lambda n=0:[self.calend(self,n),
        self.Button.config(state=DISABLED)], text="\U0001F4C6",bg = "goldenrod1")
        self.Fecha_Vencimiento.grid(padx=2,pady=2,row = 0, column = 1)
    

        #Botón de confirmar

        self.Button = Button(window, text = 'Confirmar reserva', bg= "goldenrod1", command = self.add_data)
        self.Button.grid(
            row = 30, column = 14, columnspan = 5)

        #Botón de consultar ID existente

        self.Button = Button(frame, text = 'Consultar ID', bg= "goldenrod1", command = self.add_data_Huesped)
        self.Button.grid(padx=2,pady=2,row = 9, column = 21)

        #Mensage de confirmación

        self.message = Label(text = '',bg=DGRAY, fg = 'green')
        self.message.grid(row = 32, column = 14, columnspan = 5)

        #Generar código de reserva, datos guardados

        self.Button = Button(window, text = 'Generar código de reserva',bg = "goldenrod1", command = self.code).grid(
            row = 34, column = 14, columnspan = 5)


        #Tabla contenido de Habitación (Información obtenida Programa WACO)

        frame2 = LabelFrame(self.wind, text = 'Descripción habitación',bg=DGRAY,fg="white")      
        frame2.grid(row = 5, column = 5, columnspan = 2, pady = 15, padx = 15)

        self.tree = ttk.Treeview(frame2, height = 5, columns = 2)
        self.tree.grid(row = 5, column = 5, columnspan = 2, padx= 15, pady= 15)
        
        #Los datos que se añadan acá se hacen con el llamado de la función
        # def = get_data y las variables que haya seleccionado WACO

        self.get_data()

    # def add_data_Huesped(self):

    #     query= 'SELECT * FROM Huesped' #Los datos necesarios están en la tabla de Huesped
    #     db_rows = self.run_query(query)
    #     for row in db_rows:
    #         if self.ID_Huesped == :

    #             frame = LabelFrame(self.wind, text = 'Información Principal',bg=DGRAY,fg="white")      
    #             frame.grid(row = 5, column = 17, columnspan = 2, pady = 15, padx = 15)

    #             Label(frame, text = 'Nombre: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 19)
    #             self.Nombre = Label(frame, text = self.Nombre)
    #             self.Nombre.grid(padx=2,pady=2,row = 13, column = 19)

    #             Label(frame, text = 'Apellido: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 11, column = 21)
    #             self.Apellido = Label(frame, text = self.Apellido)
    #             self.Apellido.grid(padx=2,pady=2,row = 13, column = 21)

    #             Label(frame, text = 'Correo: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 19)
    #             self.Correo = Label(frame, text = self.Correo)
    #             self.Correo.grid(padx=2,pady=2,row = 17, column = 19)

    #             Label(frame, text = 'Número telefónico: ',bg=DGRAY,fg="white").grid(padx=2,pady=2,row = 15, column = 21)
    #             self.Telefono = Label(frame, text = self.Telefono)
    #             self.Telefono.grid(padx=2,pady=2,row = 17, column = 21)

        # self.get_data()


    def run_query(self, query, parameters = ()):
        with sqlite3.connect(self.db_name) as conn:
            cursor = conn.cursor()
            result = cursor.execute(query, parameters)
            conn.commit()
        return result
    
    def get_data(self):

        # Para limpiar, no creo que deba usarse
        # records = self.tree.get_children()
        # for elements in records:
        #     self.tree.delete(elements)
            
        query= 'SELECT * FROM Habitacion' #Los datos necesarios no sólo están en la tabla de Habitacion, también los que se hayan añadido a Reserva
        db_rows = self.run_query(query)
        for row in db_rows:
            self.tree.insert('', 0, text = 'Número de habitación', 
            value= row[0]) 
            
        #Aquí el text y el value debería ser lo que seleccionó WACO en la app de él

        # text = 'Número de habitación: ', value = row[0]
        # text = 'Cupo máximo: ', value = row[]
        # text = 'Tipo habitación: ', ...
        # text = 'Fecha Ingreso: ',  ...
        # text = 'Fecha salida: ', ...
        # text = 'Cantidad de niños: ', 
        # text = 'Cantidad de adultos: ', 
        # text = 'Número de habitación: ', 
        # text = 'Valor: ', 


    def code(self): 
        
        modal = Toplevel(window)
        modal.title('Confirmación de reserva')
        modal.focus_set()
        modal.grab_set()

        modal.transient(master = window)

        label_modal = Label(modal, text="Código de reserva").pack(pady=15)

        for i in range (1):
            code = random.randint(1,100)

        self.Cod_Reserva= Label(modal, text= [code])
        self.Cod_Reserva.pack(pady=15)

        button_modal = Button(modal, text="Aceptar",command = modal.destroy and window.destroy).pack()
    
        # query = 'INSERT INTO Reserva VALUES(?)'
        # parameter = self.Cod_Reserva.cget('text') 
        # self.run_query(query, parameter) 

        # query = 'INSERT INTO Factura VALUES(?)'
        # parameter = self.Cod_Reserva.cget('text') 
        # self.run_query(query, parameter)
        
        query= 'INSERT INTO Reserva VALUES(?)' 
        db_rows = self.run_query(query)
        for row in db_rows:
            self.db_name.index(self.Cod_Reserva.cget('text'),
            value = row[0])  
        
        query= 'INSERT INTO Factura VALUES(?)' 
        db_rows = self.run_query(query)
        for row in db_rows:
            self.db_name.index(self.Cod_Reserva.cget('text'),
            value= row[0])

    def validation(self):
        return len(self.ID_Huesped.get()) != 0 and len(self.Numero_Tarjeta.get()) != 0
    
    def add_data(self):
        if self.validation():
            query = 'INSERT INTO Huesped VALUES(?, ?, ?, ?, ?, ?, ?, ?)' #Agregar la cantidad de datos necesarios para llenar la tabla
            parameters = (self.ID_Huesped.get(), self.Nombre.get(), self.Apellido.get(),
            self.Correo.get(), self.Telefono.get(), self.Tipo_Tarjeta.get(), 
            self.Numero_Tarjeta.get(), self.Fecha.cget('text'))
            self.run_query(query, parameters)
            self.message['text'] = 'La reseva ha sido exitosa'
            self.ID_Huesped.delete(0, END)
            self.Nombre.delete(0, END)
            self.Apellido.delete(0, END)
            self.Correo.delete(0, END)
            self.Telefono.delete(0, END)
            self.Tipo_Tarjeta.delete(0, END) 
            self.Numero_Tarjeta.delete(0, END)
            # self.Fecha.delete(0, END)

        else:
            self.message['text'] = 'Todos los datos son requeridos'
        
        # self.get_Huesped()

    def calend(self,N,n):
        
        calnd =Tk()
        cal=Calendar(calnd, selectmode = 'day', 
                year = 2020, month = 5, 
                day = 22)
        cal.pack()
        
        aceptar=Button(calnd,command=lambda :[self.Fecha.config(text=cal.get_date()),
        calnd.destroy(),self.Fecha_Vencimiento.config(state=NORMAL)],bg = "goldenrod1",text="Acepto",
        activebackground="green")
        aceptar.pack()





