import re
import sqlite3
import os
from sqlite3.dbapi2 import Error
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from typing import Dict
from tkcalendar import Calendar 
import Grafico_Hotel
#from interfaceValidacioncls import inicial
from PIL import ImageTk, Image

conect=False
try:
    con=sqlite3.connect("hotel.db")
    cursor=con.cursor()
except:
    print("no se pudo conectar")
else:
    print("conectado")
    conect=True

imagenes2=dict()


LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)

class sel_hab:

    def __init__(self,empleado={"id":"0","nom":"","cont":""},datos={"ckin":"00/00/00","ckout":"00/00/00","na":"0","nn":"0","nh":"0"}):

        self.empleado=empleado
        self.dat=datos
        self.ventana = Tk()
        self.ventana.title("HABITACIONES")
        wh=int((self.ventana.winfo_screenheight()-600)/2)
        ww=int((self.ventana.winfo_screenwidth()-600)/2)        
        self.ventana.geometry("600x600+"+str(ww)+"+"+str(wh))
        self.ventana.config(bg= DGRAY)
        
        for i in range(5):
            self.ventana.columnconfigure(i,weight=1)
        self.ventana.rowconfigure(0,weight=1)
        self.ventana.rowconfigure(1,weight=1)
        self.ventana.rowconfigure(2,weight=1)

        self.caja1=PanedWindow(self.ventana,bg="light gray",relief=SOLID)
        self.caja1.grid(padx=10,pady=5,row=0,column=0,columnspan=5,sticky=NSEW)
        self.caja1.columnconfigure(0,weight=1)
        self.caja1.columnconfigure(1,weight=1)
        self.caja1.columnconfigure(2,weight=1)
        self.caja1.rowconfigure(0,weight=1)
        self.caja1.rowconfigure(1,weight=1)
        self.cont1=Label(self.caja1,bg="light gray")
        self.cont1.grid(row=0,column=0)
        self.l1=Label(self.cont1, text= "FILTRAR HABITACIONES POR:", bg="light gray",font=("Verdana",10))
        self.l1.grid(row=0,column=0)
        self.lista_desplegable = ttk.Combobox(self.cont1,width=30,state="readonly")
        self.lista_desplegable.grid(row=1,column=0)
        self.lista_desplegable.set("SELECCIONE")

        self.cont2=Label(self.caja1)
        self.cont2.grid(row=0,column=1)
        self.C1 = Label(self.cont2,text = "checkin", width=10)
        self.C1.grid(row=0,column=0)
        Label(self.cont2,text=datos["ckin"]).grid(row=1,column=0)
        self.C2 = Label(self.cont2,text = "checkout", width=10)
        self.C2.grid(row=0,column=1)
        Label(self.cont2,text=datos["ckout"]).grid(row=1,column=1)
        self.C3 = Label(self.cont2,text = "habitaciones", width=10)
        self.C3.grid(row=2,column=0)
        Label(self.cont2,text=datos["nh"]).grid(row=3,column=0)
        self.C4 = Label(self.cont2,text = "personas", width=10)
        self.C4.grid(row=2,column=1)
        Label(self.cont2,text=str(int(datos["nn"])+int(datos["na"]))).grid(row=3,column=1)
        self.b1 = Button(self.caja1,command=lambda :[self.volver()], text="MODIFICAR",bg = "goldenrod1",activebackground="light green", relief=RAISED)
        self.b1.grid(padx=10,row=0,column=2)
        self.lista_desplegable["values"] = ["MAYOR PRECIO","MENOR PRECIO","CANTIDAD DE PERSONAS"]

        self.caja2=PanedWindow(self.ventana,bg="light gray",relief=SOLID,height="200")
        self.caja2.grid(padx=10,pady=5,row=1,column=0,columnspan=5,sticky=NSEW)
        self.caja2.columnconfigure(0,weight=1)
        self.caja2.rowconfigure(0,weight=1)
        self.caja2.rowconfigure(1,weight=1)

        self.tree = ttk.Treeview(self.caja2)
        self.tree.grid(row = 0, column = 0, padx= 10, pady= 10)
        #self.llenar()
        self.llenar1()
        
    def llenar1(self):
        habs=cursor.execute("Select * from Habitacion")
        habs=habs.fetchall()

        contenedor=Canvas(self.caja2)
        frame=Frame(contenedor)
        scbr=Scrollbar(self.caja2,orient=VERTICAL,command=contenedor.yview)
        
        contenedor.configure(yscrollcommand=scbr.set)
        frame.grid(row=0,column=0,sticky=NSEW)
        contenedor.columnconfigure(0,weight=1)
        scbr.grid(row=0,column=1,sticky=NS)
        contenedor.grid(row=0,column=0,sticky=NSEW)

        contenedor.create_window((1,2), window=frame, anchor="center",tags="frame")

        def onFrameConfigure(canvas):
            '''Reset the scroll region to encompass the inner frame'''
            canvas.configure(scrollregion=canvas.bbox("all"))

        frame.bind("<Configure>", lambda event, canvas=contenedor: onFrameConfigure(canvas))
   
        seleccion=StringVar()
        rw=0
        filas=[]
        for i in habs:
            filas.append(Label(frame))
            filas[rw].grid(row=rw,sticky=NSEW)
            for j in range(6):
                filas[rw].columnconfigure(j,weigh=1)
            Radiobutton(filas[rw], text=i[0],width="10", variable=seleccion,value=i[0],relief=GROOVE).grid(row=0,column=0,sticky=NSEW)
            Label(filas[rw],text=i[1],width="10",relief=GROOVE).grid(row=0,column=1,sticky=NSEW)
            Label(filas[rw],text=i[2],width="10",relief=GROOVE).grid(row=0,column=2,sticky=NSEW)
            Label(filas[rw],text=i[3],width="10",relief=GROOVE).grid(row=0,column=3,sticky=NSEW)
            Label(filas[rw],text=i[4],width="10",relief=GROOVE).grid(row=0,column=4,sticky=NSEW)
            Label(filas[rw],text=i[5].replace(";","\n"),width="2",relief=GROOVE).grid(row=0,column=5,sticky=NSEW)
            rw+=1
      

        
    def llenar(self):
            habs=cursor.execute("Select * from Habitacion")
            habs=habs.fetchall()
            self.tree.config(height=len(habs) ,columns=("#0","#1","#2","#3","#4","#5"))
            self.tree.heading("#0", text="Habitacion")
            self.tree.heading("#1", text="Cupo Maximo")
            self.tree.heading("#2", text="Tipo Habitacion")
            self.tree.heading("#3", text="Estado")
            self.tree.heading("#4", text="Precio noche")
            self.tree.heading("#5", text="Servicios")

            self.tree.column("#0", width=80, anchor='c')
            self.tree.column("#1", width=90, anchor='c')
            self.tree.column("#2", width=90, anchor='c')
            self.tree.column("#3", width=80, anchor='c')
            self.tree.column("#4", width=90, anchor='c')
            self.tree.column("#5", width=90, anchor='c')
            for i in habs:
                self.tree.insert('', 0, text = i[0], 
                values= (i[1],i[2],i[3],i[4],i[5].replace(';',"\n"))) 
            
    
    def volver(self):
        import interfaceValidacioncls
        empleado=self.empleado
        dat=self.dat
        self.ventana.destroy()
        print("volver",empleado)
        interfaceValidacioncls.inicial(empleado,dat)
    
    def reserv(self):
        id_hab=str(self.lista_habitacion1.get(ACTIVE))
        self.ventana.destroy
        Grafico_Hotel.Huesped(id_hab,self.empleado,self.dat)
       
