class Mtransporte():
    color=""
    tamaño=""
    marca=""
    velocidad=0
    cupos=0
    pasajeros=0

    def __init__(self,icolor="",itamaño="",imarca="",ivelocidad=0,icupos=0,ipasajeros=0):
        self.color=icolor
        self.tamaño=itamaño
        self.marca=imarca
        self.velocidad=ivelocidad
        self.cupos=icupos
        self.pasajeros=ipasajeros

    def pintar(self,Ncolor):
        self.color=Ncolor
    
    def acelerar(self,Nvelocidad):
        self.velocidad=Nvelocidad

    def frenar(self):
        self.velocidad=0
    
    def subirPasajeros(self,NP):
        if self.cupos<=self.pasajeros()+NP:
            self.pasajeros+=NP
        else:
            print("No hay cupo")
    
    def bajarPasajeros(self,NP):
        if 0<=self.pasajeros()-NP:
            self.pasajeros-=NP
        elif self.pasajeros>0:
            self.pasajeros=0
        else:
            print("No hay pasajeros")

    
