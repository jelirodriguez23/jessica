import os
import random

os.system("cls")

n=random.randint(1,4)
m=random.randint(1,4)
p=random.randint(1,4)

matA=[]
matB=[]

for i in range(n):
    matA.append([])
    for j in range(m):
        matA[i].append(random.randint(-50,50))

for i in range(m):
    matB.append([])
    for j in range(p):
        matB[i].append(random.randint(-50,50))

print("\nMatriz A: ",n,"X",m)

for i in matA:
    print(i)

print("\nMatriz B: ",m,"X",p)

for i in matB:
    print(i)
    
matC=[]


for i in range(n):
    matC.append([])
    for j in range(p):
        op=0
        for k in range(m):
            op+=(matA[i][k]*matB[k][j])
        matC[i].append(op)

print("\nMatriz resultante")
for i in matC:
    print(i)
