from tkinter import *
from tkinter import ttk
import sqlite3
from tkinter import messagebox
import os

LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)

logueado=False

class Busquedad_Reserva:

    db_name = 'Hotel.db'
    
    def __init__(self, v5, empl={"id":"0","nom":"","cont":""}):
        self.empl=empl
        # CodigoRes=StringVar()
        # IngresoIdH=StringVar()
        v5.iconbitmap('moon.ico')
        self.res=[]
        self.textx=Label(v5,bg=RGRAY,fg='white',text="Ingrese el Código Reserva o Id-Huesped",font=("Ebrima",10))
        self.textx.place(x=80,y=20)

        self.CodReserva=Label(v5,text="Codigo Reserva:",bg=RGRAY,fg='white', font=("Ebrima",12))
        self.CodReserva.place(x=20,y=50)
        self.IngresoCod=Entry(v5,font=("Courier",12))
        self.IngresoCod.place(x=180, y=50)

        self.IdHuesped=Label(v5,text="Id-Huesped:",bg=RGRAY,fg='white', font=("Ebrima",12))
        self.IdHuesped.place(x=20,y=80)
        self.IngresoId=Entry(v5,font=("Courier",12))
        self.IngresoId.place(x=180, y=80)

        self.Botbuscar=Button(v5,text="Buscar",command=lambda :self.validar(self.IngresoCod.get(),self.IngresoId.get(),v5),width=20, height=1, anchor="center",bd=0,bg = "goldenrod1",fg = "black",font=("Ebrima",10),relief=RAISED)
        self.Botbuscar.place(x=40,y=120)

        self.Botbuscar=Button(v5,text="volver",command=lambda :self.volver(v5),width=20, height=1, anchor="center",bd=0,bg = "goldenrod1",fg = "black",font=("Ebrima",10),relief=RAISED)
        self.Botbuscar.place(x=220,y=120)
        
        v5.mainloop()

    def buscarR(self,cod_res):
        #print("select * from Reserva where Cod_Reserva = \""+cod_res+"\"")
        res=cursor.execute("select * from Reserva where Cod_Reserva = "+str(cod_res)+"")
        reserva=res.fetchall()[0]
        self.res=reserva
        print(cod_res,reserva)
        hab=cursor.execute("select * from Habitacion where Numero_Habitacion = \""+reserva[6]+"\"")
        habitacion=hab.fetchall()[0]
        print(habitacion)
        hue=cursor.execute("select * from Huesped where ID_Huesped = \""+reserva[7]+"\"")
        huesped=hue.fetchall()[0]
        print(huesped)
        
        v6 =Tk()
        v6.title('Datos de la Reservas')
        v6.iconbitmap('moon.ico')
        wh=int((v6.winfo_screenheight()-400)/2)
        ww=int((v6.winfo_screenwidth()-560)/2)
        v6.geometry("560x400+"+str(ww)+"+"+str(wh))
        v6.config(bg=RGRAY, relief='raised', bd=0,highlightthickness=0)
        
        labelN =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Nombre-Apellido")
        labelN.place(x=10,y=20)
        labelDN=Label(v6, text=huesped[1]+" "+huesped[2])
        labelDN.place(x=10,y=50)
        labelCR =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Código Reserva")
        labelCR.place(x=15,y=80)
        labelC=Label(v6, text = reserva[0])
        labelC.place(x=10,y=110)

        labelCheckIn =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Check-In")
        labelCheckIn.place(x=167,y=20)
        labelCI=Label(v6, text =reserva[1])
        labelCI.place(x=150,y=50)
        labelChekOut =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Check-Out")
        labelChekOut.place(x=165,y=80)
        labelCO=Label(v6, text = reserva[2])
        labelCO.place(x=150,y=110)

        labelHab =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "No.Habitación")
        labelHab.place(x=297,y=20)
        labelH=Label(v6, text = reserva[6])
        labelH.place(x=290,y=50)
        labelPrec =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Precio")
        labelPrec.place(x=315,y=80)
        labelP=Label(v6, text = habitacion[4])
        labelP.place(x=290,y=110)

        labelEst =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Estado")
        labelEst.place(x=457,y=20)
        labelP=Label(v6, text = reserva[5])
        labelP.place(x=457,y=50)


        labelInf=Label(v6,text="INFORMACIÓN DE CONFIRMACIÓN PARA CAMBIOS",bg = "goldenrod1",width=62, height=1,fg='black', font=("Ebrima",12))
        labelInf.pack(anchor=CENTER)
        labelInf.place(x=0,y=150)
        
        labelEmail =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Email")
        labelEmail.place(x=10,y=190)
        labelEm=Label(v6, text = huesped[3])
        labelEm.place(x=10,y=220)
        labelCelu =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Celular")
        labelCelu.place(x=10,y=250)
        labelCel=Label(v6, text = huesped[4])
        labelCel.place(x=10,y=280)
        labelNumCre =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Tarjeta Credito")
        labelNumCre.place(x=10,y=310)
        labelNC=Label(v6, text = huesped[6])
        labelNC.place(x=10,y=340)
        

        # labelCambios =Label(v6, bg=RGRAY,fg='white',font=("Ebrima",10),text = "Cambios")
        # labelCambios.place(x=250,y=190)
        # listadesplegable=ttk.Combobox(v6,width=25)
        # listadesplegable.place(x=250, y=220)
        # opciones=["Enviar carta de confirmación","Modificar días de reserva","Modificar Habitación"]
        # listadesplegable['values']=opciones
        # BotRealizar=Button(v6,text="Realizar",width=20, height=1, anchor="center",bd=0,bg = "goldenrod1",fg = "black",font=("Ebrima",10))
        # BotRealizar.place(x=260,y=250) 
        BotRealizar=Button(v6,text="cancelar",command=lambda :self.cancel(v6) ,width=20, height=1, anchor="center",bd=0,bg = "goldenrod1",fg = "black",font=("Ebrima",10))
        BotRealizar.place(x=260,y=250)
            
        BotRealizar=Button(v6,text="volver",command=lambda :self.volver(v6) ,width=20, height=1, anchor="center",bd=0,bg = "goldenrod1",fg = "black",font=("Ebrima",10))
        BotRealizar.place(x=260,y=300)

    def validar(self,h1,h2,v5):
        print(h1,h2,v5)
        Id_Huesped=h2
        Codi_Reserva=int(h1)
        acceso=False
        for us in users:
            if (Id_Huesped in us) and (Codi_Reserva in us):
                print("loo")
                v5.destroy()          
                acceso=True
                global logueado
                self.buscarR(Codi_Reserva)
                logueado=True
        if not(acceso):
            messagebox.showerror(title="INGRESO",message="No existe esta reserva")

    def datos(self,n1):
        print(n1)
        nombre=n1
        for us in users2:
            us1=print(us[0])
    
    def volver(self,vent):
        import interfaceValidacioncls
        vent.destroy()
        interfaceValidacioncls.inicial(self.empl)

    def cancel(self,v):
        id_res=self.res[0]
        cursor.execute("UPDATE Reserva set Estado_Reserva=\"Cancelada\" where Cod_Reserva="+str(id_res))
        con.commit()
        self.volver(v)
        
conect=False
try:
    con=sqlite3.connect("Hotel.db")
    cursor=con.cursor()
except:
    print("no se pudo conectar")
else:
    print("conectado")
    conect=True

if conect:
    user=cursor.execute("Select * from Reserva")
    users=user.fetchall()
    user2=cursor.execute("Select * from Huesped")
    users2=user2.fetchall()
    print(users)
    print(users2)



