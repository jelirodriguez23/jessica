import sqlite3
import os
from sqlite3.dbapi2 import Error
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from tkcalendar import Calendar 
import Buscar
from PIL import ImageTk, Image
from datetime import date, datetime

conect=False
try:
    con=sqlite3.connect("hotel.db")
    cursor=con.cursor()
except:
    print("no se pudo conectar")
else:
    print("conectado")
    conect=True

imagenes2=dict()


LGRAY = '#3e4042' # button color effects in the title bar (Hex color)
DGRAY = '#25292e' # window background color               (Hex color)
RGRAY = '#10121f' # title bar color                       (Hex color)

class inicial:

    def __init__(self,empl={"id":"0","nom":"","cont":""},datos={"ckin":"00/00/00","ckout":"00/00/00","na":"0","nn":"0","nh":"0"}):
        self.empleado=[]
        self.logueado=False
        if( empl["nom"] != "" and empl["cont"]!=""):
            prov=Tk()
            self.validar(empl["nom"],empl["cont"],prov)
        else:
            self.logg()
        
        if self.logueado:
            self.ind=Tk()
            self.ind.title(" --> INDEX <--")
            self.ind.config(bg=DGRAY)
            self.ind.iconbitmap('moon.ico')
           
            wh=int((self.ind.winfo_screenheight()-600)/2)
            ww=int((self.ind.winfo_screenwidth()-600)/2)
            self.ind.geometry("600x600+"+str(ww)+"+"+str(wh))
            self.ind.resizable(height=False, width=False)

            self.tit=PanedWindow(self.ind,relief=RIDGE,bg="light gray")
            self.tit.columnconfigure(0,weight=1)
            self.tit.columnconfigure(1,weight=1)
            
            self.img_logo=ImageTk.PhotoImage(Image.open("logo.png"))
            self.Logo=Label(self.tit,image=self.img_logo, width=113,height=102,bg="light gray")
            self.Logo.grid(padx=3, pady=3, row=0,column=0,rowspan=4)
            Label(self.tit,bg="light gray",text="HOTEL 360 ").grid(row=1,column=1)
            Label(self.tit,bg="light gray",text="El mejor lugar para descansar ").grid(row=2,column=1)
            Label(self.tit,bg="light gray").grid(row=3,column=3,pady=1,padx=50)

            self.nper=Label(self.ind,text=str(int(datos["na"])+int(datos["nn"])),relief=SUNKEN)
            self.na=Label(self.ind,text=datos["na"],relief=SUNKEN)
            self.nn=Label(self.ind,text=datos["nn"],relief=SUNKEN)
            self.b_nper=Button(self.ind,command=lambda :[self.npersonas()],text="+",bg = "goldenrod1",activebackground="green")
            self.nhab=Label(self.ind,text=datos["nh"],relief=SUNKEN)
            self.b_nhabs=Button(self.ind,command=lambda :[self.nhab.config(text=str(int(self.nhab.cget("text"))+1))],text="+",bg = "goldenrod1",activebackground="green")
            self.b_nhabr=Button(self.ind,command=lambda :[self.nhab.config(text=str(int(self.nhab.cget("text"))-1)) if (int(self.nhab.cget("text")) > 1) else False],text="-",bg = "goldenrod1",activebackground="green")
            self.ckin=Label(self.ind,text=datos["ckin"],relief=SUNKEN)
            self.b_ckin=Button(self.ind,command=lambda n=0:[self.b_ckin.config(state=DISABLED),self.calend(n)],bg = "goldenrod1",text="\U0001F4C6")
            self.ckout=Label(self.ind,text=datos["ckout"],relief=SUNKEN)
            self.b_ckout=Button(self.ind,command=lambda n=1:[self.b_ckout.config(state=DISABLED),self.calend(n)],bg = "goldenrod1",text="\U0001F4C6")

            self.b_bdisp=Button(self.ind,command=lambda :[self.dispo()],text="Disponibilidad",bg = "light green",activebackground="green") 
            self.b_breser=Button(self.ind,command=lambda :[self.reser()],text="Buscar",bg = "light blue",activebackground="green") 
            #p_info=PanedWindow()

            self.imgs=dict()
            n=0
            for i in os.listdir("imgs"):
                self.imgs[n]=ImageTk.PhotoImage(Image.open("imgs\\"+i))
                n+=1
            n=0
            self.f_fondoimg=Label(self.ind,bg="light gray")
            self.f_img=Label(self.f_fondoimg, image=self.imgs[0],text=0, width=350,height=250,bg="light blue")
            self.b_next=Button(self.f_fondoimg,command= lambda :[self.f_img.config(image=self.imgs[int((self.f_img.cget("text"))+1)%len(self.imgs)],text=(int(self.f_img.cget("text"))+1)%len(self.imgs)) ],bg="goldenrod1",text=">")
            self.b_prev=Button(self.f_fondoimg,command= lambda :[self.f_img.config(image=self.imgs[int((self.f_img.cget("text"))-1)%len(self.imgs)],text=(int(self.f_img.cget("text"))-1)%len(self.imgs)) ],bg="goldenrod1",text="<")

            
            for i in range(10):
                self.ind.columnconfigure(i,weight=1)
            for i in range(8,11):
                self.ind.rowconfigure(i,weight=1)

            self.tit.grid(pady=5,row=1,column=1,columnspan=11,sticky=NSEW)
            Label(self.ind,bg=DGRAY).grid(row=2,column=12,pady=3,padx=7)
            Label(self.ind,text="# Personas",bg=DGRAY,fg="white").grid(pady=2, row=3,column=1,columnspan=2,sticky=NSEW)
            self.nper.grid(row=4,column=1,sticky=NSEW)
            self.b_nper.grid(row=4,column=2)
            Label(self.ind,text="# Habitaciones",bg=DGRAY,fg="white").grid(pady=2, row=3,column=3,columnspan=3,sticky=NSEW)
            self.b_nhabr.grid(row=4,column=3)
            self.nhab.grid(row=4,column=4,sticky=NSEW)
            self.b_nhabs.grid(row=4,column=5)
            Label(self.ind,text="Fecha ingreso",bg=DGRAY,fg="white").grid(pady=2, row=3,column=6,columnspan=2,sticky=NSEW)
            self.ckin.grid(row=4,column=6,sticky=NSEW)
            self.b_ckin.grid(row=4,column=7)
            Label(self.ind,text="Fecha salida",bg=DGRAY,fg="white").grid(pady=2, row=3,column=8,columnspan=2,sticky=NSEW)
            self.ckout.grid(row=4,column=8,sticky=NSEW)
            self.b_ckout.grid(row=4,column=9)
            self.b_bdisp.grid(padx=5, row=3,column=11)
            self.b_breser.grid(row=4,column=11)
            Label(self.ind,bg=DGRAY).grid(row=5,column=12,pady=7,padx=7)
            self.f_fondoimg.grid(row=6,column=1,columnspan=11,rowspan=4,sticky=NSEW,pady=5)
            self.f_fondoimg.columnconfigure(1,weight=1)
            self.f_fondoimg.rowconfigure(0,weight=1)
            self.f_img.grid(row=0,column=1,sticky=NSEW)
            self.b_prev.grid(row=0,column=0,sticky=NS)
            self.b_next.grid(row=0,column=2,sticky=NS)

            #p_info.grid(padx=20,pady=12,row=6,column=0,rowspan=4,columnspan=11,sticky=(N, S, E, W))
            self.ind.mainloop()
            con.close()
   
    def f_fechas(self,f1="00/00/00",f2="00/00/00",function="-"):
        if(function=="-"):
            dats1=f1.split("/")
            dats2=f2.split("/")
            dif=(int(dats2[2])-int(dats1[2]))*365
            dif+=(int(dats2[1])-int(dats1[1]))*30
            dif+=(int(dats2[0])-int(dats1[0]))
            print(dats1)
            print(dats2)
            print(dif)
            return dif

    def calend(self,n):
        now = datetime.now()
        calnd =Tk()
        cal=Calendar(calnd, selectmode = 'day', 
                year = now.year, month = now.month, 
                day = now.day)
        cal.pack()
        if n==0:
            aceptar=Button(calnd,command=lambda :[self.ckin.config(text=cal.get_date()),calnd.destroy(),self.b_ckin.config(state=NORMAL)],text="Acepto",activebackground="green")
            aceptar.pack()
        else:
            aceptar=Button(calnd,command=lambda :[self.ckout.config(text=cal.get_date()) if self.f_fechas(self.ckin.cget("text"),cal.get_date())>0 else messagebox.showerror(title="Fecha",message="Fecha de salida anterior a fecha de ingreso"),
                            calnd.destroy(),self.b_ckout.config(state=NORMAL)],text="Acepto")
            aceptar.pack()
        calnd.protocol("WM_DELETE_WINDOW", lambda:[calnd.destroy(),self.b_ckin.config(state=NORMAL),self.b_ckout.config(state=NORMAL)])

    def logg(self):

        root=Tk()
        root.title(" --> LOGIN <--")
        root.config(bg=RGRAY,bd=0,highlightthickness=0)
        wh=int((root.winfo_screenheight()-260)/2)
        ww=int((root.winfo_screenwidth()-400)/2)
        root.geometry("400x180+"+str(ww)+"+"+str(wh))
        root.resizable(height=False, width=False)
        root.iconbitmap('moon.ico')

        root.columnconfigure(0,weight=0)
        root.columnconfigure(1,weight=1)

        etiq1=Label(root,text="LOGGIN EMPLEADO", bg=RGRAY,fg="white")
        etiq1.grid(padx=3,pady=15,row=0,column=0,columnspan=2)

        etiq1=Label(root,text="USUARIO:", bg=RGRAY,fg="white")
        etiq1.grid(padx=7,pady=3,row=1,column=0)

        etiq2=Label(root,text="CONTRASEÑA:", bg=RGRAY,fg="white")
        etiq2.grid(padx=7,pady=3,row=2,column=0)

        e1=Entry(root)
        e1.grid(row=1,column=1)

        e2=Entry(root,show="*")
        e2.grid(row=2,column=1)

        b1=Button(root, text="Ingresar",command=lambda :self.validar(e1.get(),e2.get(),root),width="20" ,bg = "goldenrod1",fg = "black", activebackground="green",relief=RAISED)
        b1.grid(pady=20,row=4,column=0,columnspan=2)

        root.mainloop()

    def npersonas(self):
        self.b_nper.config(state=DISABLED)
        niños=int(self.nn.cget("text"))
        adultos=int(self.na.cget("text"))
        p1=Frame()
        p1.config(width="150",height="300",bd=2,relief=RIDGE,bg="light gray")
        p1.place(x=5,y=200)
        la=Label(p1,text="Adultos",bg="light gray").grid(row=0,column=0,columnspan=3)
        lna=Label(p1,text=str(adultos),relief=SUNKEN)
        lna.grid(row=1,column=1)
        bnas=Button(p1,text="+",bg = "goldenrod2",command=lambda :[lna.config(text=str(int(lna.cget("text"))+1)),self.nper.config(text=str(int(self.nper.cget("text"))+1))],activebackground="green").grid(row=1,column=2)
        bnar=Button(p1,text="-",bg = "goldenrod2",command=lambda :[lna.config(text=str(int(lna.cget("text"))-1)) if ( int(lna.cget("text"))>0 ) else False, self.nper.config(text=str(int(lnn.cget("text"))+int(lna.cget("text"))))],activebackground="green").grid(row=1,column=0)
        ln=Label(p1,text="niños",bg="light gray").grid(row=2,column=0,columnspan=3)
        lnn=Label(p1,text=str(niños),relief=SUNKEN)
        lnn.grid(row=3,column=1)
        bnns=Button(p1,text="+",bg = "goldenrod2",command=lambda :[lnn.config(text=str(int(lnn.cget("text"))+1)),self.nper.config(text=str(int(self.nper.cget("text"))+1))],activebackground="green").grid(row=3,column=2)
        bnnr=Button(p1,text="-",bg = "goldenrod2",command=lambda :[lnn.config(text=str(int(lnn.cget("text"))-1)) if ( int(lnn.cget("text"))>0 ) else False, self.nper.config(text=str(int(lnn.cget("text"))+int(lna.cget("text"))))],activebackground="green").grid(row=3,column=0)
        ba=Button(p1,text="Aceptar",bg = "goldenrod2",command=lambda :[self.nn.config(text=lnn.cget("text")),self.na.config(text=lna.cget("text")),p1.destroy(),self.b_nper.config(state=NORMAL)],activebackground="green").grid(row=4,column=0)
        bc=Button(p1,text="Cancelar",bg = "goldenrod2",command=lambda :[self.nper.config(text=str(niños+adultos)),p1.destroy(),self.b_nper.config(state=NORMAL)],activebackground="red").grid(row=4,column=2)
    
    def validar(self,nombre,contr,root):
        cosn=cursor.execute("Select * from Empleado Where nombre=\""+nombre+"\"")
        empl=cosn.fetchall()
        
        acceso=False
        if (len(empl)>0):
            if(empl[0][3]==contr):
                self.empleado=empl[0]
                root.destroy()           
                self.logueado=True
            else:
                messagebox.showerror(title="INGRESO",message="contraseña erronea")
        else:
            messagebox.showerror(title="INGRESO",message="No existe empleado con ese nombre")

    def dispo(self):
        if(self.f_fechas(self.ckin.cget("text"),self.ckout.cget("text"))>0):
            if(int(self.nper.cget("text"))>0 and int(self.nhab.cget("text"))>=1 ):
                import Reservar_hab
                empl={
                    "id":self.empleado[0],
                    "nom":self.empleado[1],
                    "cont":self.empleado[3]
                }
                dat={
                    "ckin":self.ckin.cget("text"),
                    "ckout":self.ckout.cget("text"),
                    "na":self.na.cget("text"),
                    "nn":self.nn.cget("text"),
                    "nh":self.nhab.cget("text")
                }
                self.ind.destroy()
                print("datos para siguiente",dat,empl)
                hab=Reservar_hab.sel_hab(empl,dat)
            else:
                messagebox.showerror(title="Datos",message="Debe seleccionar al menos una habitacion \n y debe haber almenos una persona")
    
        else:
            messagebox.showerror(title="Fecha",message="La fecha de ingreso debe ser \n anterior a la fecha de salida")
    
    def reser(self):
        empl={
            "id":self.empleado[0],
            "nom":self.empleado[1],
            "cont":self.empleado[3]
        }
        self.ind.destroy()
        v5= Tk()
        v5.title('Busquedad de reservas')
        v5.iconbitmap("")
        wh=int((v5.winfo_screenheight()-360)/2)
        ww=int((v5.winfo_screenwidth()-600)/2)
        v5.geometry("400x180+"+str(ww)+"+"+str(wh))
        v5.config( bg=RGRAY, relief='raised', bd=0,highlightthickness=0)
        v5.resizable(0, 0) 
        aplication = Buscar.Busquedad_Reserva(v5,empl)
        v5.mainloop()

