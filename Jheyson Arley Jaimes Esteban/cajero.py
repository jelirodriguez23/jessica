import os #llama a la libreria Operate System
os.system("cls") #Llamamos la funcion cls: clear screen

valor=int(input("Ingrese valor a retirar: ")) #lee el valor a retirar

if(valor<=1000 and valor%10==0): #verifica si el valor es menor a 1000 y es multiplo de 10 
    n50=int(valor/50) #calcula la cantidad maxima de billetes de 50
    vtemp=valor-(n50*50)
    if( n50>1 and (vtemp>=50 or vtemp==0)):
        n50-=1
    valor=valor-(n50*50) #calcular cuanto dinero falta
    n20=int(valor/20) #calcula la cantidad maxima de billetes de 20
    valor=valor-(n20*20) #calcula cuanto dinero falta
    n10=int(valor/10) #calcula la cantidad maxima de  billetes de 10
    print("se entregan: \n",n50,"billetes de 50, \n",n20,"billetes de 20, \n",n10,"billetes de 10") #impri cuantos billetes de cada denominacion entrega
else:
    print("el valor dado no es valido") #si el valor no cumple los requisito imprime que no es balido 