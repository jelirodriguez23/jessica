import os

os.system("cls")
def fun1(A,B,C):
    R=A|B
    R=R&C
    print("\n  (A \U000022C3 B) \U000022C2 C = ",R,"\n")
    return True

def fun2(A,B,C):
    R=A&B
    R=R-C
    print("  (A \U000022C2 B) - C = ",R,"\n")
    return True

def fun3(A,B,C):
    R=B|C
    R=A-R
    print("  A - (B \U000022C2 C) = ",R,"\n")
    return True

def salir(A,B,C):
    return False

seg=True

while seg:
    A={1,2,3,4,5,6,7,8,9,10}
    B={1,2,3,5,8,13,21,34,55}
    C={2,4,6,8,10,12,14,16,18}
    oper={1:fun1,2:fun2,3:fun3,4:salir}
    print("Menu principal:\n","A=",A,"\n B=",B,"\n C=",C,"\n\n 1. (A \U000022C3 B) \U000022C2 C \n 2. (A \U000022C2 B) - C \n 3. A - (B \U000022C3 C) \n 4. salir")
    r=int(input("\n Ingrese la opcion: "))
    
    if (r>0) and (r<5):
        seg=oper[r](A,B,C)
    else:
        print("Opcion no valida")
        
    if seg:
        input("presione cualquier tecla para continuar...")
        os.system("cls")