from genericpath import isdir
import os
import time
import shutil

os.system("cls")

# rut1=input("Ingrese la ruta y nombre de la carpeta 1: ")
# rut2=input("Ingrese la ruta y nombre de la carpeta 2: ")

rut1="d:\\Users\\ESTUDIANTE\\Desktop\\git\\jessica\\Jheyson Arley Jaimes Esteban"
rut2="d:\\Users\\ESTUDIANTE\\Desktop\\git\\jessica"

def leerArch(txt):
    leido=False
    while not(leido):
        Carp= str(input(txt))
        if (os.path.exists(Carp) == False): 
            print(" ///////////// Direccion Erronea /////////////")
            print("Intenta de nuevo.")
        elif(not(os.path.isdir(Carp))):
            print("La ruta especificada no corresponde a una carpeta.")
            print("Intenta de nuevo.")
        else:
            leido=True
    return Carp

def leer(txt="Ingrese una opcion",rang=(0,100)):
    leido=False
    while not(leido):
        try:
            opcion = int(input(txt))
            print()
        except ValueError:
            print("/////// Dato no valido /////////")
            print("intenta de nuevo")
        else:
            if opcion<=rang[1] and opcion>=rang[0]:
                leido=True
            else:
                print("valor fuera de rango intenta de nuevo")
    return opcion

def tam(rut):
    rut=str(rut)
    t=0
    if(os.path.isdir(rut)):
        cont1=os.listdir(rut)
        for i in cont1:
            if(os.path.isdir(rut+"\\"+i)):
                t+=tam(rut+"\\"+i)
            else:
                t+=os.path.getsize(rut+"\\"+i)
    else:
        t+=os.path.getsize(rut)
    return(t)

def archivooos(rut1,rut2):
    if(os.path.exists(rut1) and os.path.exists(rut2)):

        info1=os.stat(rut1)
        arch1=os.path.basename(rut1)
        cont1=os.listdir(rut1)

        info2=os.stat(rut2)
        arch2=os.path.basename(rut2)
        cont2=os.listdir(rut2)
        
        print("_"*97)
        print("|",arch1.center(45),"|",arch2.center(45),"|")
        print("|","_"*93,"|")
        print("|",str(tam(rut1)/1000).center(45),"|",str(tam(rut2)/1000).center(45),"|")
        print("| create at",str(time.ctime(info1.st_ctime)).center(35),"| create at",str(time.ctime(info2.st_ctime)).center(35),"|")
        print("| update at",str(time.ctime(info1.st_atime)).center(35),"| update at",str(time.ctime(info2.st_atime)).center(35),"|")
        print("|","_"*93,"|")
        ma=max(len(cont1),len(cont2)) 
        
        for i in range(ma):
            try:
                d1=rut1+"\\"+cont1[i]
                img1="\U0001F4C4"
                if(os.path.isdir(d1)):
                    img1="\U0001F4C1"
                sd1=str(tam(d1)/1000)+" Kb"
                d1=" "+img1+" "+cont1[i].ljust(30)+" "+sd1
            except:
                d1="\U00002796"
            try:
                d2=rut2+"\\"+cont2[i]
                img2=" \U0001F4C4"
                if(os.path.isdir(d2)):
                    img2="\U0001F4C1"
                sd2=str(tam(d2)/1000)+" Kb"
                d2=" "+img2+" "+cont2[i].ljust(30)+" "+sd2
            except:
                d2=" \U00002796"
            
            print("|",d1.ljust(44),"|",d2.ljust(44),"|")

        print("_"*97)
        
        # print("contenido "+arch1+":\n ",info1)
        # print("contenido "+arch2+":\n ",info2)
    else:
        print("no se encontro alguna de las rutas")

def comparar_archivos (cp1,cp2):
    cont = 0
    with os.scandir(cp1) as archivos:
        for archivo1 in archivos:
            with os.scandir(cp2) as archivos2:
                for archivo2 in archivos2:
                    if (archivo1.name == archivo2.name):
                        cont +=1
                        print("La archivo repetido es:",archivo1.name)
    print("Achivos repetidos: ", cont)

def copiar(carp1,carp2):
    carp={0:carp1,1:carp2}
    print("0. ",carp1,"\n1. ",carp2)
    n=leer("selecciones el origen:",(0,1))
    orig=carp[n]
    dest=carp[(n+1)%2]
    ops=os.listdir(orig)
    for i in range(len(ops)):
        print(i,". ",ops[i],)
    print()
    arch=ops[leer("ingrese el nombre del archivo copiar: ",(0,len(ops)))]

    if not(os.path.exists(dest+"\\Copia "+os.path.basename(orig))):
        os.mkdir(dest+"\\Copia "+os.path.basename(orig),77777)
    shutil.copy(orig+"\\"+arch,dest+"\\Copia "+os.path.basename(orig)+"\\"+arch)
    print("Achivos copiado correctamente ")


continuar = True
while continuar == True:    
    cp1=leerArch("Ingrese ruta de la carpeta 1: ")
    cp2=leerArch("Ingrese ruta de la carpeta 2: ")
    if(cp1 == cp2):
        c = input("SON LA MISMA CARPETA, DESEA CONTINUAR? (S/N): ")
        if (c.upper() == 'S'):
            continuar  =  False
            os.system("cls")
    else:
        continuar=False


menu = {1:archivooos,2:comparar_archivos,3:copiar}
seguir = True

while( seguir ==  True):
    with os.scandir(cp1) as archivos:
        print("\n INFORMACION GENERAL:")
        print("-"*(90))
        print("| CARPETA NUMERO 1:",os.path.basename(cp1),""*(4)," | Tamaño:",tam(cp1))
        print("| Catidad de archivos: ",len(os.listdir(cp1)))
        print("| Creado:",time.ctime(os.path.getctime(cp1)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp1)))
        print("-"*(90))

        
    with os.scandir(cp2) as archivos2:
        print("-"*(90))
        print("| CARPETA NUMERO 2:",os.path.basename(cp2),""*(4)," | Tamaño:",tam(cp2))
        print("| Catidad de archivos: ",len(os.listdir(cp2)))
        print("| Creado:",time.ctime(os.path.getctime(cp2)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp2)))
        print("-"*(90))

    print("\t MENU PRINCIPAL \n"+"1.Mostrar Archivos \n"+"2.Buscar Archivos repetidos\n"+"3.Copiar archivos entre carpetas\n"+"4.Finalizar")      
    opcion=leer("\n Digite la operacion deseada: ",(1,4))
    if opcion != 4: 
        menu[opcion](cp1,cp2)
    else:
        print("PROGRAMA FINALIZADO...")
        seguir = False

    input("Presone para volver")
    os.system("cls")
