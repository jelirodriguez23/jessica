from tkinter import ttk 
from tkinter import*

ventana = Tk()
ventana.title("HABITACIONES")
ventana.geometry("600x600+100+100")
ventana.config(bg= "SteelBlue1")

v1 = Frame()
v1.config(width=580, height= 130, bg= "navajo white",bd=2, relief=RIDGE)
v1.place(x=10,y=20)
l1=Label(v1, text= "FILTRAR HABITACIONES POR:", bg="peach puff",font=("Verdana",10))
l1.place(x=30,y=50)

lista_desplegable = ttk.Combobox(v1,width=30,state="readonly")
lista_desplegable.place(x=30,y=70)
lista_desplegable.set("SELECCIONE")

lista_desplegable["values"] = ["MAYOR PRECIO","MENOR PRECIO","CANTIDAD DE PERSONAS"]

C1 = Label  (v1,text = "checkin", width=10)
C1.place(x=280,y=70)

C2 = Label(v1,text = "checkout", width=10)
C2.place(x=370,y=70)

C3 = Label(v1,text = "habitaciones", width=10)
C3.place(x=280,y=100)

C4 = Label(v1,text = "personas", width=10)
C4.place(x=370,y=100)

b1 = Button(v1, text="MODIFICAR", bg="light gray",activebackground="light green", relief=RAISED).place(x=460,y=68)

C5 = Label(ventana,text = "Hora Reserva final", width=20, height= 5, bd=2,bg="light gray")
C5.place(x=320,y=450)

b2 = Button(ventana, text="RESERVAR", bg="light gray",activebackground="light green", relief=RAISED).place(x=500,y=500)

img = PhotoImage(file='h1.png')
widget = Label(ventana, image=img)
widget.pack()
widget.grid(padx=10, pady=300)


b3 = Button(ventana, text=">", bg="light gray",activebackground="light green", relief=RAISED).place(x=270,y=380)
 

ventana.mainloop()