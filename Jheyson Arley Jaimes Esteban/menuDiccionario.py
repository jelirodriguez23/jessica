
import os

os.system("cls")

def sum(n1,n2):
    print(n1,"+",n2,"=",n1+n2)


def mult(n1,n2):
    print(n1,"*",n2,"=",n1*n2)

def div(n1,n2):
    if n2==0:
        print("division entre cero no permitida")
    else:
        print(n1,"+",n2,"=",n1/n2)

dicc={1:sum,2:mult,3:div}
seguir=True
while seguir:
    print("\nMenu principal:\n 1) Sumar dos umeros \n 2) Multiplicar dos numeros \n 3) Dividir dos numeros \n 4) Finalizar")
    try:
        r=int(input("Ingrese opcion: "))
        if r>4 or r<=0:
            print("opcion no valida")
        elif r==4:
            seguir=False
        else:
            a=int(input("\nIngrese a: "))
            b=int(input("Ingrese b: "))
            dicc[r](a,b)
    except Exception as error:
           print("a ocurrido el siguiente error:", error)