import os
import json

def entradaDatos():
    misDatos["empleados"] = []
    continuar = True
    while (continuar):
        empleado = {}
        print("POR FAVOR DIGITE LOS DATOS DEL EMPLEADO")

        doc = input("Digite documento ")
        nom = input("Digite nombre ")
        edad = input("Digite edad ")
        afp = input("Digite afp ")
        eps = input("Digite eps ")
        arl = input("Digite arl ")

        empleado["numDoc"] = doc
        empleado["nombre"] = nom
        empleado["edad"] = edad
        empleado["AFP"] = afp
        empleado["EPS"] = eps
        empleado["ARL"] = arl

        misDatos["empleados"].append(empleado)
        #empleados.append(empleado)
        c = input("DESEA REGISTRAR OTRO EMPLEADO? (S/N)")
        continuar = (c.upper()=="S")
    f = open("data.json","w")
    json.dump(misDatos,f,indent=4)

def buscarRegistro(cedula_b):
    for i in range (len(misDatos["empleados"])):
        if misDatos["empleados"][i]["numDoc"] == str(cedula_b):
            print("Encontrado")
            return(misDatos["empleados"][i])

def borrarRegistro(cedula_b):
    i = 0
    encontrado = False
    while not encontrado and i < len(misDatos["empleados"]):
        if misDatos["empleados"][i]["numDoc"] == str(cedula_b):
            print("Encontrado")
            encontrado = True
            print(misDatos["empleados"][i])
            respuesta = input("Desea eliminar el registro encontrado? (S/N): ")
            if respuesta.upper() == "S":
                misDatos["empleados"].pop(i)
        i = i + 1
    if not encontrado:
        print("No se encontro el registro")

#PROGRAMA PRINCIPAL
os.system("cls")
misDatos = {}
entradaDatos()

print(misDatos["empleados"])
ced = str(input("Digite la Cedula a BUSCAR: "))
registro = buscarRegistro(ced)
# print(registro)

print(misDatos["empleados"])
print()
ced = str(input("Digite la Cedula a BORRAR: "))
borrarRegistro(ced)
print(misDatos["empleados"])

