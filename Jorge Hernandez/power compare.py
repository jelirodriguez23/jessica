# Jorge Hernandez Oviedo
# Cristian Zuluaga
# Dana Marcela Ramirez Niño
# Juan Pablo Lozada

import os
import os, time

def lista (a,b):
    print ("\n\tCarpeta 1")
    for dirpath, dirnames, filenames in os.walk(a):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            print (fp)
    print ("\n\tCarpeta 2")
    for dirpath, dirnames, filenames in os.walk(b):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            print (fp)
def tamaño(a,b):
    tam_a = 0
    tam_b = 0
    for dirpath, dirnames, filenames in os.walk(a):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            tam_a += os.path.getsize(fp)
    for dirpath, dirnames, filenames in os.walk(b):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            tam_b += os.path.getsize(fp)
    print ("El tamaño de la carpeta 1 es:",tam_a)
    print ("El tamaño de la carpeta 2 es:",tam_b)
    if tam_a==tam_b:
        print ("Las carpetas tienen el mismo tamaño")
    else:
        print ("Las carpetas tienen distinto tamaño")
def nombre(a,b):
    arc_1=os.path.basename(a)
    arc_2=os.path.basename(b)
    print ("La carpeta 1 se llama:", arc_1)
    print ("La carpeta 2 se llama:", arc_2)
    if arc_1==arc_2:
        print ("Las carpetas tienen el mismo nombre")
    else:
        print ("Las carpetas tienen distinto nombre")
def fecha (a,b):
    print ("\n\20 1. Fecha de creacion \n\20 2. Fecha modificacion")
    opc=int(input("Digite el numero de la accion que desea realizar "))
    if opc==1:
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime)=os.stat(a)
        aa=os.stat(a)
        print ("La carpeta 1 se creo: %s" %time.ctime(ctime))
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime)=os.stat(b)
        bb=os.stat(b)
        print ("La carpeta 2 se creo: %s" %time.ctime(ctime))
        if aa==bb:
            print ("Las carpetas tienen igual fecha de creacion")
        else:
            print ("Las carpetas tienen distinta fecha de creacion")
    if opc==2:
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime)=os.stat(a)
        aa=os.stat(a)
        print ("La carpeta 1 se modifico: %s" %time.ctime(mtime))
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime)=os.stat(b)
        bb=os.stat(b)
        print ("La carpeta 2 se modifico: %s" %time.ctime(mtime))
        if aa==bb:
            print ("Las carpetas tienen igual fecha de modificacion")
        else:
            print ("Las carpetas tienen distinta fecha de modificacion")
def comparar(a,b):
    print ("\n\20 1. Por nombre \n\20 2. Por tamaño")
    opc=int(input("Digite el numero de la accion que desea realizar "))
    t1=list()
    t_1=list()
    t2=list()
    t_2=list()
    for dirpath, dirnames, filenames in os.walk(a):
        for f in filenames:
            fp=os.path.join(dirpath, f)
            f_p=os.path.join(f)
            t1.append(fp)
            t_1.append(f_p)
    for dirpath, dirnames, filenames in os.walk(b):
        for g in filenames:
            fq=os.path.join(dirpath, g)
            f_q=os.path.join(g)
            t2.append(fq)
            t_2.append(f_q)
        
    if opc==1:
        print("\n Estos archivos son similares")
        for k in t_1:
            for j in t_2:
                if k==j:
                    print (k,"=",j)
                else:
                    print ("No hay archivos con el mismo nombre")
    if opc==2:
        print("\n Estos archivos son similares")
        print ("Archivo|Tamaño=Archivo|Tamaño")
        for k in t1:
            for j in t2:
                if os.path.getsize(k)==os.path.getsize(j):
                    print (k,"|",os.path.getsize(k),"=",j,"|",os.path.getsize(j))
                else:
                    print ("No hay archivos con el mismo tamaño")
    
            
os.system("cls")
lv=True
car_1=input("Ingrese la ruta de la carpeta 1 ")
car_2=input("Ingrese la ruta de la carpeta 2 ")
while lv:
    try:
        print ("\n\t\6 MENU PRINCIPAL \6 \n\20 1. Conocer los nombres de las carpetas"
        "\n\20 2. Conocer el tamaño de las carpetas"
        "\n\20 3. Conocer fecha de creacion / modificacion  \n\20 4. Conocer listado de carpetas"
        "\n\20 5. Comparar archivos de las carpetas \n\20 6. Salir")
        opc=int(input("Digite el numero de la accion que desea realizar "))
        ope={1:nombre, 2:tamaño, 3:fecha, 4:lista, 5:comparar,}
        while opc<=6 and opc>0:
            if opc==6:
                print ("\t¡Fin!")
                lv=False
                break
            else:
                ope[opc](car_1, car_2)
                break
    except ValueError:
        print ("NO es un numero")
    except:
        print ("Error no definido")