#A parti de una matrix cuadrada de dimension 8X8 tipo tabla de ajedrez
#Elabore un programa python que recibe una posicion  y determine todas las posiciones a las que puede acceder un caballo
#desde la posicion indicada.
import random
import os
os.system("cls")

#Declaramos variables que se van a necesitar
f=b=8
bandera =1
matriz=[]

# Pedimos los datos de fila y columna 
x=int(input("Ingrese la posición de fila de [1-8] "))-1
y=int(input("Ingrese la posición de la columna de [1-8] "))-1

# Creamos una matriz de 8 por 8 para luego ubicar la posición del tercero
print()
print(" -> Primero creamos la matriz resultante la ideal con vacios.. ")
tabla = []
for i in range(f):
    tabla.append([])
    for j in range(f):
        tabla[i].append(" ")

#Asignamos a la posición del caballo el valor de 2 para identificarlo
tabla[x][y]="♞"

#Se asignan las posiciones posibles del caballo y se almacenan en una lista
x1=[[x+2,y-1],[x+2,y+1],[x+1,y+2],[x+1,y-2],[x-2,y+1],[x-2,y-1],[x-1,y-2],[x-1,y+2]]

for i in x1:
    if (i[0] >= 0 and i[0] < 8 and i[1] >= 0 and i[1] < 8):
        tabla[i[0]][i[1]]='♟'

#Mostramos la matriz con la posición del caballo
print("El resultado es: ")
for i in range(8):   
    print(tabla[i])
