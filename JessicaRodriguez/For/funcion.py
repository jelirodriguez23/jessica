
# funcion f(x) = x^2 -2x + 1
# dado los valores de x entre -10 y 10 poblar al tabla
x=0

for i in range (-10,11):
    x = (i**2)-2*i+1
    print(f"Cuando x vale {i} la f(x)  vale  {x}")

