from conexiones import conexbd
from tkinter import messagebox

def GuardarEmpleados(tidoDoc,numDoc,nombre,apellidos):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()
    print(tidoDoc,numDoc,nombre,apellidos)
    statement = f"SELECT IdEmpleado from Empleados WHERE NumeroDocumento ={numDoc} AND TipoDocumento like '%{tidoDoc}%';"
    cur.execute(statement)
    if cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos ya existen.')
    else:
        sql = f"INSERT INTO Empleados(TipoDocumento,NumeroDocumento,Nombres,Apellidos) VALUES ('{tidoDoc}','{numDoc}','{nombre}','{apellidos}');"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron guardados exitosamente.')
    
def ActualizarEmpleados(tidoDoc,numDoc,nombre,apellidos):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdEmpleado from Empleados WHERE NumeroDocumento ={numDoc} AND TipoDocumento like '%{tidoDoc}%';"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para actualizar.')
    else:
        sql = f"UPDATE Empleados SET Nombres= '{nombre}',Apellidos='{apellidos}' WHERE TipoDocumento='{tidoDoc}' AND NumeroDocumento='{numDoc}';"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Actualizados exitosamente.')
    
def EliminarEmpleados(tidoDoc,numDoc):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdEmpleado from Empleados WHERE NumeroDocumento ={numDoc} AND TipoDocumento like '%{tidoDoc}%';"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para eliminar.')
    else:
        sql = f"DELETE FROM Empleados WHERE TipoDocumento='{tidoDoc}' AND NumeroDocumento='{numDoc}';"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Eliminados exitosamente.')