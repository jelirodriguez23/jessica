from os import terminal_size
import sqlite3 
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from conexiones import conexbd
from clases.clsEmpleados import *
import datetime
from tkinter import font

 
ventana = tk.Tk()
ventana.configure(bg ="#ffda9e")
ventana.iconbitmap("QuadBike.ico")
ancho_ventana = 700
alto_ventana = 300
x_ventana = ventana.winfo_screenwidth() // 2 - ancho_ventana // 2
y_ventana = ventana.winfo_screenheight() // 2 - alto_ventana // 2
posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)

ventana.geometry(posicion)
ventana.resizable(0, 0)

class Empleados(tk.Frame):
    def empleadosG():
        lista = []
        tipoDoc = IngTipoDoc.get()
        doc = IngNumeroDocumento.get()
        name = IngNombres.get()    
        fullName = IngApellidos.get()
        if not tipoDoc:
            lista.append(tipoDoc)
        if not doc:
            lista.append(doc)
        if not doc:
            lista.append(name)
        if not doc:
            lista.append(fullName)
        x = str(lista)
        print(x)
        if len(lista) >= 1:
            messagebox.showinfo("Error","Los datos ingresados no son válidos, debe llenar todos los campos.")
        else:
            print(tipoDoc,doc,name,fullName)
            GuardarEmpleados(tipoDoc,doc,name,fullName)
        

    def empleadosE():
        tipoDoc = IngTipoDoc.get()
        doc = IngNumeroDocumento.get()
        EliminarEmpleados(tipoDoc,doc)


    def empleadosM():
        tipoDoc = IngTipoDoc.get()
        doc = IngNumeroDocumento.get()
        name = IngNombres.get()    
        fullName = IngApellidos.get()
        ActualizarEmpleados(tipoDoc,doc,name,fullName)

    def empleadosC():
        tipoDoc = IngTipoDoc.get()
        doc = IngNumeroDocumento.get()
        name = IngNombres.get()    
        fullName = IngApellidos.get()

    def empleadosL():
        IngTipoDoc.set("")
        IngNumeroDocumento.set("")
        IngNombres.set('')
        IngApellidos.set('')


ventana.title("Empleados")

x = datetime.datetime.now()
hora = x
TimeNow =Label(ventana, text=f"FECHA: {hora}",bg="#ffda9e",font=("Segoe UI", 10), foreground="black" ,justify=tk.CENTER).pack()
Titulo =Label(ventana, text="EMPLEADOS",bg="#ffda9e",font=("Segoe UI", 20 ,font.BOLD), foreground="#b186f1",justify=tk.LEFT ).pack()


lblf = LabelFrame(ventana,text="Datos Personales")
lblf.place(x=100, y=100, width=500, height=100)

TipoDoc =Label(ventana, text="Tipo Documento:",justify='center',font=("Segoe UI", 10) )
TipoDoc.place(x=110,y=120)

opciones = ["Cédula","Pasaporte","Tarjeta de identidad","Cédula de extranjería"]

IngTipoDoc = ttk.Combobox(ventana, font=("Segoe UI", 8),width=40,state="readonly", values= opciones)
IngTipoDoc.place(x=220,y=120, width=100, height=20)
IngTipoDoc.set("")

NumeroDocumento =Label(ventana, text="Numero Documento:",font=("Segoe UI", 10))
NumeroDocumento.place(x=340,y=120)

IngNumeroDocumento= tk.Entry(ventana,font=("Segoe UI", 8))
IngNumeroDocumento.place(x=470,y=120, width=100, height=20)

Nombres =Label(ventana, text="Nombres:",font=("Segoe UI", 10))
Nombres.place(x=110,y=170)

IngNombres= tk.Entry(ventana,font=("Segoe UI", 8))
IngNombres.place(x=180,y=170, width=130, height=20)

Apellidos =Label(ventana, text="Apellidos:",font=("Segoe UI", 10))
Apellidos.place(x=330,y=170)

IngApellidos= tk.Entry(ventana,font=("Segoe UI", 8))
IngApellidos.place(x=400,y=170, width=130, height=20)

Entrar=Button(ventana, text="Guardar", command=Empleados.empleadosG,font=("Segoe UI", 10),foreground="#610B5E", width=10)
Entrar.place(x=100,y=220)

Entrar=Button(ventana, text="Eliminar", command=Empleados.empleadosE,font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=200,y=220)

Entrar=Button(ventana, text="Modificar", command=Empleados.empleadosM,font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=300,y=220)

Entrar=Button(ventana, text="Consultar", command=Empleados.empleadosC,font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=400,y=220)

Entrar=Button(ventana, text="Limpiar", command=Empleados.empleadosL,font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=500,y=220)

ventana.mainloop()