
import sqlite3 as sqlite

class conexion:

    bd_name= 'parqueadero.db'
    
    def __init__(self):
        try:
            self.conex= sqlite.connect(self.bd_name, uri=True)
        except ValueError:
            print("Error en la conexion.")
        
    def curbd(self):
        try:
            return self.conex.cursor()
        except ValueError:
            print("Error en el cursor.")

    def commitdb(self):
        try:
            con = self.conex
            return con
        except ValueError:
            print("Error en el cursor.")


 # self.connect = sqlite.connect("C:/Users/jelir/OneDrive/Documentos/Project python/Uniminuto/parqueadero.db") 
 # return self.connect.cursor()
