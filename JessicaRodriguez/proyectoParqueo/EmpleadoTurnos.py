import sqlite3
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from conexiones import conexbd
from clases.clsEmpleTurnos import *
import datetime
import time
from tkcalendar import Calendar, DateEntry
import tkinter.font as TkFont



ventana = Tk()
ventana.configure(bg ="#ffda9e")
ventana.iconbitmap("QuadBike.ico")
# Mostar centrado el formulario
ancho_ventana = 700
alto_ventana = 600
x_ventana = ventana.winfo_screenwidth() // 2 - ancho_ventana // 2
y_ventana = ventana.winfo_screenheight() // 2 - alto_ventana // 2
posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
ventana.geometry(posicion)
ventana.resizable(0, 0)
# Titulo de la ventana de windows
ventana.title("Calendarios de empleados")

#Crear el formato de la tabla
class Table(tk.Frame):
    def __init__(self, parent=None, title="", headers=[], height=10, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self._title = tk.Label(self, width=40,text=title, background="#ECCCCE", font=("Helvetica", 16), foreground="purple")
        #ancho del titulo en la grilla
        self._headers = headers
        self._tree = ttk.Treeview(self,
                                  height=height,
                                  columns=self._headers, 
                                  show="headings")
        self._title.pack(side=tk.TOP, fill="x")

        # Agregamos dos scrollbars 
        vsb = ttk.Scrollbar(self, orient="vertical", command=self._tree.yview)
        vsb.pack(side='right', fill='y')
        hsb = ttk.Scrollbar(self, orient="horizontal", command=self._tree.xview)
        hsb.pack(side='bottom', fill='x')

        self._tree.configure(xscrollcommand=hsb.set, yscrollcommand=vsb.set)
        self._tree.pack(side="left")

        for header in self._headers:
            self._tree.heading(header, text=header.title())
            self._tree.column(header, stretch=True,
                              width = TkFont.Font().measure(header.title()))

    def add_row(self, row):
        self._tree.insert('', 'end', values=row)
        for i, item in enumerate(row):
            col_width = TkFont.Font().measure(item)
            if self._tree.column(self._headers[i], width=None) < col_width:
                    self._tree.column(self._headers[i], width=140)
            #ancho de las columnas en la grilla


def pintar():
    #Pintar la tablita

    clientes_headers  = tk.font.Font(font='bold')  
    clientes_headers = (u"Empleado", u"Jornada", u"Canlendario", u"Horario")
    clientes_tab = Table(ventana, title="Datos del turno", headers=clientes_headers)
    clientes_tab.place(x=100, y=260)
    data = get_data()
    for row in data:
            clientes_tab.add_row(row)

def empleadosTurnosG():
    lista = []
    idEmpleado = IngEmpleado.get()
    idTurno = IngTurno.get()
    fechaIniTur = IngFecIni.get()
    fechaFinTur = IngFecFin.get()
   
    if not idEmpleado or (idEmpleado == "Seleccione.."):
        lista.append(idEmpleado)
    if not idTurno or (idTurno =="Seleccione.."):
        lista.append(idTurno)
    if not fechaIniTur:
        lista.append(fechaIniTur)
    if not fechaFinTur:
        lista.append(fechaFinTur)
    if len(lista) >= 1:
        messagebox.showinfo(
            "Error", "Los datos ingresados no son válidos, debe llenar los campos requeridos")
    else:
        for i in rtaEmp:
            if idEmpleado == i[1]:
                idEmpleado = i[0]
        for y in rtaTur:
            if idTurno == y[1]:
                idTurno = y[0]
        GuardarEmpleTur(idEmpleado, idTurno, fechaIniTur, fechaFinTur)
    pintar()

def empleadosTurnosE():
    idEmpleado = IngEmpleado.get()
    idTurno = IngTurno.get()
    lista = []
    idEmpleado = IngEmpleado.get()
    idTurno = IngTurno.get()
    fechaIniTur = IngFecIni.get()
    fechaFinTur = IngFecFin.get()
    if not idEmpleado or (idEmpleado == "Seleccione.."):
        lista.append(idEmpleado)
    if not idTurno or (idTurno =="Seleccione.."):
        lista.append(idTurno)
    if not fechaIniTur:
        lista.append(fechaIniTur)
    if not fechaFinTur:
        lista.append(fechaFinTur)
    if len(lista) >= 1:
        messagebox.showinfo(
            "Error", "Los datos ingresados no son válidos, debe llenar los campos requeridos.")
    else:
        for i in rtaEmp:
                if (idEmpleado == i[1]):
                    idEmpleado = i[0]
        for y in rtaTur:
                if (idTurno == y[1]):
                    idTurno = y[0]
        EliminarEmpleTur(idEmpleado, idTurno)
        pintar()

def empleadosTurnosM():
    idEmpleado = IngEmpleado.get()
    idTurno = IngTurno.get()
    fechaIniTur = IngFecIni.get()
    fechaFinTur = IngFecFin.get()
    lista = []
    if not idEmpleado or (idEmpleado == "Seleccione.."):
        lista.append(idEmpleado)
    if not idTurno or (idTurno =="Seleccione.."):
        lista.append(idTurno)
    if len(lista) >= 1:
        messagebox.showinfo(
            "Error", "Los datos ingresados no son válidos, debe llenar los campos requeridos.")
    else:
        for i in rtaEmp:
                if (idEmpleado == i[1]):
                    idEmpleado = i[0]
        for y in rtaTur:
                if (idTurno == y[1]):
                    idTurno =y[0]
        ActualizarEmpleTur(idEmpleado, idTurno, fechaIniTur, fechaFinTur)
        pintar()

def empleadosTurnosC():
    pintar()

def empleadosTurnosL():
    IngEmpleado.set("Seleccione..")
    IngTurno.set("Seleccione..")
    #IngFecFin.
    pintar()

def listarEmp():
    lista = []
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    sql = f"SELECT IdEmpleado, Nombres ||' ' || Apellidos as nombreEmp from Empleados;"
    cursor = cur.execute(sql)
    for i in cursor:
        lista.append(i)
    return lista

def listarTurno():
    lista = []
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    sql = f"SELECT IdTurno, Jornada from Turnos;"
    cursor = cur.execute(sql)
    for i in cursor:
        lista.append(i)
    return lista
    
def get_data():
    # Para limpiar, no creo que deba usarse  
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()
    query= "SELECT Nombres || '  ' || Apellidos as Empleados , Jornada,  FechaInicioTurno ||' - '|| FechaFinTurno as Caledario, HoraInicio || ' - ' ||HoraFinal as horario FROM EmpleadosTurnos et join Empleados e on et.IdEmpleado=e.IdEmpleado join Turnos t on et.IdTurno=t.IdTurno;"
    #Los datos necesarios no sólo están en la tabla de Habitacion, también los que se hayan añadido a Reserva
    db_rows = cur.execute(query)
    return(db_rows)


x = datetime.datetime.now()
hora = x
TimeNow =Label(ventana, text=f"FECHA: {hora}",bg="#ffda9e",font=("Segoe UI", 10), foreground="black",justify=tk.CENTER).pack()
Titulo = Label(ventana, text="Calendario de empleados",bg="#ffda9e", font=("Segoe UI", 20, TkFont.BOLD),foreground="#b186f1",justify=tk.CENTER ).pack()

lblf = LabelFrame(ventana , text="Datos Personales")
lblf.place(x=100, y=100, width=500, height=100)

Empleado = Label(ventana, text="Empleado:",justify='center', font=("Segoe UI", 10))
Empleado.place(x=110, y=120)

rtaEmp = listarEmp()
listaValor = []
for i in rtaEmp:
    listaValor.append(i[1])

IngEmpleado = ttk.Combobox(ventana, font=("Segoe UI", 8), values=listaValor)
IngEmpleado.place(x=190, y=120, width=130, height=20)
IngEmpleado.set("Seleccione..")

Turno = Label(ventana, text="Turno:", font=("Segoe UI", 10))
Turno.place(x=330, y=120)

rtaTur = listarTurno()
listaJornada = []
for i in rtaTur:
    listaJornada.append(i[1])

IngTurno = ttk.Combobox(ventana, font=("Segoe UI", 8), values=listaJornada)
IngTurno.place(x=380, y=120, width=130, height=20)
IngTurno.set("Seleccione..")


#top = tk.Toplevel(ventana)
FechaInicio = Label(ventana, text="Fecha Inicio:", font=("Segoe UI", 10))
FechaInicio.place(x=110, y=170)

IngFecIni = DateEntry(ventana, width=12, background='darkpurple',foreground='white', borderwidth=2)
IngFecIni.place(x=190, y=170, width=130, height=20)

FechaFin = Label(ventana, text="Fecha Fin:", font=("Segoe UI", 10))
FechaFin.place(x=330, y=170)

IngFecFin = DateEntry(ventana, width=12, background='darkpurple',foreground='white', borderwidth=2)
IngFecFin.place(x=400, y=170, width=130, height=20)

Entrar = Button(ventana, text="Guardar", command=empleadosTurnosG,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=100, y=220)

Entrar = Button(ventana, text="Eliminar", command=empleadosTurnosE,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=200, y=220)

Entrar = Button(ventana, text="Modificar", command=empleadosTurnosM,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=300, y=220)

Entrar = Button(ventana, text="Consultar", command=empleadosTurnosC,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=400, y=220)

Entrar = Button(ventana, text="Limpiar", command=empleadosTurnosL,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=500, y=220)


ventana.mainloop()
