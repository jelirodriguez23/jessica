import random
import os
os.system("cls")

pe=0
def hanoii(n,o="a",t="b",d="c"):
    global pe
    if n == 1:
        print(o,"---->",d)
        pe += 1
    else:
        hanoii(n-1,o,t,d)
        hanoii(1,o,d,t)
        hanoii(n-1,t,d,o)

n = int(input("número de discos "))   
hanoii(n)
print("El número de movimientos ",pe)

