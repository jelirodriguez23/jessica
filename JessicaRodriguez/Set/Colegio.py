# Elabrorar un programa python que lea los nombres de los integrantes de cada seleccion deportiva 
# de los dos que tienen el colegio.
# construya una funicon de lectura para las dos secciones deportivas

#Mostrar nombre de estudiantes que:

'''
a) Solo juegan
b) solo juegan baloncesto
c) jueguen futbol o baloncesto
d) particen en las dos selecciones al tiempo
'''
global listanames1
listanames1= []
global listanames2
listanames2 = []
lecturaValida = False

def integrantes(inte,p):
    for i in range(inte):
        nombres = input("Ingrese el nombre del integrante: ")
        if p==1:
            listanames1.append(nombres)
        if p==2:
            listanames2.append(nombres)
            
while (not lecturaValida):
    try:
        p=1 
        inte1 = int(input("Ingrese la cantidad de integrantes de la selección de futbol: "))
        integrantes(inte1,p)
        lecturaValida = True
    except IOError:
        print("No es un número")
    except ValueError:
        print("Ha value error: error no es un número. ")
    except ZeroDivisionError:
        print("Ha ocurrido un error en la divicion, por cero")
    except :
        print("Error desconocido")
    else:
        print("No hay error. ")
        
p=2
inte2= int(input("Ingrese la cantidad de integrantes de la selección de baloncesto: "))
integrantes(inte2,p)

x = set(listanames1)
y = set(listanames2)


print()
print("Solo Juegan Fútbol")
print(x-y)
print()
print("Solo Juegan basquet")
print(y-x)
print()
print("Integrantes de ambos equipos")
print(x|y)
print()
print("Integrantes que juegan en los dos equipos al mismo tiempo ")
print(x & y)


''' 
for i in listanames1:
    print("Lista Futbol",i)

for i in listanames2:
    print("Lista Basquet",i)
'''