a = {1,2,3,4,}
b = {5,6,7,8,4,3}
a|b = {1,2,3,4,5,6,7,8} # union aUb
a&b = {4,3}  # anb
a-b = {2,4} #a-b
b-a = {5,6,7,8} #b-a
a^b = {2,4,5,6,7,8} #aVb