''''
Poblar una lista con valores entregados por el ususario mostrar la lista
formada.
El tamaño de la lista es elegible por el usuario.
'''
#Limpiar la consola
import os
os.system("cls")

miLista = []
n = int(input("De que tamaño quiere la lista: "))
for i in range(n):
    x = input("Ingrese el valor: ")
    miLista.append(x)

Lista2 = [2,3,4,5,6]
miLista.extend(Lista2)    
print(miLista,  end= " ")
miLista.remove(3)
print("\n", miLista,  end= " ")
miLista.pop(3)
print("\n", miLista,  end= " ")