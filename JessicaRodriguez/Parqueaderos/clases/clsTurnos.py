from conexiones import conexbd
from tkinter import messagebox

def GuardarTurnos(jornada,horaInicio,horaFin):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdTurno from Turnos WHERE Jornada = '{jornada}';"
    cur.execute(statement)
    if cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='La jornada ya se encuentra registrada.')
    else:
        sql = f"INSERT INTO Turnos(Jornada,HoraInicio,HoraFinal) VALUES ('{jornada}','{horaInicio}','{horaFin}');"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron guardados exitosamente.')
    
def ActualizarTurnos(jornada,horaInicio,horaFin):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdTurno from Turnos WHERE Jornada = '{jornada}';"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para actualizar.')
    else:
        sql = f"UPDATE Turnos SET HoraInicio= '{horaInicio}',HoraFinal='{horaFin}' WHERE Jornada='{jornada}';"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Actualizados exitosamente.')
    
def EliminarTurnos(jornada):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdTurno from Turnos WHERE Jornada = '{jornada}';"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para Eliminar.')
    else:
        sql = f"DELETE FROM Turnos WHERE Jornada = '{jornada}';"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Eliminados exitosamente.')