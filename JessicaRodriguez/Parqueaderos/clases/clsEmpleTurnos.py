from conexiones import conexbd
from tkinter import messagebox

def GuardarEmpleTur(idEmpleado,idTurno,fechaIniTur,fechaFinTur):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()
    statement = f"SELECT IdEmpleadoTurno from EmpleadosTurnos WHERE IdEmpleado ={idEmpleado} AND IdTurno = {idTurno};"
    cur.execute(statement)
    if cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos ya existen.')
    else:
        sql = f"INSERT INTO EmpleadosTurnos(IdEmpleado,IdTurno,FechaInicioTurno,FechaFinTurno) VALUES ({idEmpleado},{idTurno},'{fechaIniTur}','{fechaFinTur}');"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron guardados exitosamente.')
    
def ActualizarEmpleTur(idEmpleado,idTurno,fechaIniTur,fechaFinTur):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdEmpleadoTurno from EmpleadosTurnos WHERE IdEmpleado ={idEmpleado} AND IdTurno = {idTurno};"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para actualizar.')
    else:
        sql = f"UPDATE EmpleadosTurnos SET FechaInicioTurno= '{fechaIniTur}',FechaFinTurno='{fechaFinTur}' WHERE  IdEmpleado ={idEmpleado} AND IdTurno = {idTurno};"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Actualizados exitosamente.')
    
def EliminarEmpleTur(idEmpleado,idTurno):
    conn = conexbd.conexion()
    cur = conn.curbd()
    com = conn.commitdb()

    statement = f"SELECT IdEmpleadoTurno from EmpleadosTurnos WHERE IdEmpleado ={idEmpleado} AND IdTurno = {idTurno};"
    cur.execute(statement)
    if not cur.fetchone():  # An empty result evaluates to False.
       messagebox.showinfo(title='Error',message='Los datos No existen para eliminar.')
    else:
        sql = f"DELETE FROM EmpleadosTurnos WHERE  IdEmpleado ={idEmpleado} AND IdTurno = {idTurno};"
        cur.execute(sql)
        com.commit()
        com.close()
        messagebox.showinfo(title='Mensaje',message='Los datos fueron Eliminados exitosamente.')


        # recorrer el fetchone para tomar caa dato de cada caja y validar con if si el campo es diferetne de los que trae la consulta
        # inicialmente , entonces seteee el campo y se agrega la coma
        # pero siempre los parametros condicionales ssería los id de la tabla de 
