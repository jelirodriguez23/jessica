from os import terminal_size
import sqlite3 
from sqlite3.dbapi2 import Row
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from tkinter import font
from conexiones import conexbd
from clases.clsTurnos import *
import datetime

ventana = Tk()
ventana.configure(bg ="#ffda9e")
ancho_ventana = 700
alto_ventana = 300
x_ventana = ventana.winfo_screenwidth() // 2 - ancho_ventana // 2
y_ventana = ventana.winfo_screenheight() // 2 - alto_ventana // 2
posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)

ventana.geometry(posicion)
ventana.resizable(0, 0)
ventana.title("Turnos")

def TurnosG():
    lista = []
    jornada = IngJornada.get()
    horaInicio = IngHoraInicio.get()
    horaFin = IngHoraFin.get()    

    if not jornada:
       lista.append(jornada)
    if not horaInicio:
       lista.append(horaInicio)
    if not horaFin:
           lista.append(horaFin)
    x = str(lista)
    print(x)
    if len(lista) >= 1:
        messagebox.showinfo("Error","Los datos ingresados no son válidos, debe llenar todos los campos.")
    else:
        GuardarTurnos(jornada,horaInicio,horaFin)

def TurnosE():
    jornada = IngJornada.get()
    EliminarTurnos(jornada)


def TurnosM():
    jornada = IngJornada.get()
    horaInicio = IngHoraInicio.get()
    horaFin = IngHoraFin.get() 
    ActualizarTurnos(jornada,horaInicio,horaFin)

def TurnosC():
    jornada = IngJornada.get()
    horaInicio = IngHoraInicio.get()
    horaFin = IngHoraFin.get()    

def TurnosL():
    
    IngHoraInicio.set("")
    IngHoraFin.set("")  

def obtener_info(): # Definimos una funcion para obtener informacion
    #obtener elemento seleccionado
    print(IngJornada.get()) #obtiene el valor de la casilla
    # print(lista_desplegable.current()) #obtiene la posicion de la casilla

x = datetime.datetime.now()
hora = x
TimeNow =Label(ventana, text=f"FECHA: {hora}",bg="#ffda9e",font=("Segoe UI", 10, font.BOLD), foreground="black", justify=tk.CENTER).pack()
Titulo =Label(ventana, text="TURNOS",bg="#ffda9e",font=("Segoe UI", 20), foreground="#b186f1",justify=tk.LEFT ).pack()
#Titulo.place(x=250,y=40)

lblf = LabelFrame(text="Datos del turno:")
lblf.place(x=100, y=100, width=500, height=100)

Caja2=Label(ventana, text= "Elija la jornada laboral", font=("Segoe UI", 10) )
Caja2.place(x=200,y=120)


# #OPCION PREDETERMINADA
# #lista de opciones
opciones = ["Mañana","Tarde","Noche"]

IngJornada = ttk.Combobox(ventana,width=20,state="readonly", values= opciones)
IngJornada.place(x=350,y=120)
IngJornada.set(".....")
# #insertar valores


HoraInicio =Label(ventana, text="Hora de inicio:",justify='center',font=("Segoe UI", 10) )
HoraInicio.place(x=110,y=150)

IngHoraInicio= tk.Entry(ventana ,font=("Segoe UI", 8))
IngHoraInicio.place(x=220,y=150, width=100, height=20)

HoraFin =Label(ventana, text="Hora de fin:",font=("Segoe UI", 10))
HoraFin.place(x=330,y=150)

IngHoraFin= tk.Entry(ventana,font=("Segoe UI", 8))
IngHoraFin.place(x=460,y=150, width=100, height=20)

Entrar = Button(ventana, text="Guardar", command=TurnosG,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=100, y=220)

Entrar = Button(ventana, text="Eliminar", command=TurnosE,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=200, y=220)

Entrar = Button(ventana, text="Modificar", command=TurnosM,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=300, y=220)

Entrar = Button(ventana, text="Consultar", command=TurnosC,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=400, y=220)

Entrar = Button(ventana, text="Limpiar", command=TurnosL,
                font=("Segoe UI", 10), foreground="#610B5E", width=10)
Entrar.place(x=500, y=220)




ventana.mainloop()