'''Simular la entrega de billetes de un cajero electronico bajo las siguientes condiciones

Se debe leer la cantidad de dinero a despachar
El monto a entregar debe ser múltiplo de 10.000
El cajero posee solo billetes de 10.000 , 20.000 y 50.000
El monto maximo a despachar es de 1'000.000
El cajero entrega siempre la mas alta cantidad de billetes posibles de la mas alta denominación

los miles se muestran como 10.000 = 10K
'''
bandera = 1

while (bandera > 0):
    print("INICIO DE LA TRANSACCION")
    retirarDinero = int(input("Ingrese el valor a retirar $"))

    b1 = 0
    b2 = 0
    b5 = 0
     
    if retirarDinero < 10000  or retirarDinero >= 1000000:
        print("ERROR .. El monto solicitado no se puede retirar, vuelva a intentarlo.")
        break
    elif (retirarDinero % 10000) != 0:
        print("ERROR .. La cantidad solicitada no es múltiplo de 10.000, la transacción es fallida.")
        break
    else:
        dineroRestante = retirarDinero
        #ORDENAMOS DE MAYOR A MENOR DENOMINACION DE BILLETES
        while dineroRestante > 0:
            #print("saldo",dineroRestante)
            while dineroRestante >= 50000:
                b5 += 1
                dineroRestante = dineroRestante -  50000
            while dineroRestante >= 20000:
                b2 += 1
                dineroRestante = dineroRestante - 20000
            while dineroRestante >= 10000:
                b1 += 1
                dineroRestante = dineroRestante - 10000

    print("TRANSACCIÓN REALIZADA CON EXITO!!!")
    print("  La salida del dinero, fue la siguiente:")
    if b5 >= 1:
        print(f" {b5} billete de $50.000")
    if b2 >= 1:
        print(f" {b2} billete de $20.000")
    if b1 >= 1:
        print(f" {b1} billete de $10.000")
    print(f"Para un total de ${retirarDinero}")
    bandera = 0
exit()