#Calcular el factorial de un número dado
print("Que operación quieres hacer? ")
print("---------------------------------------- ")
opc = int(input("1: El factorial, 2: Sumatoria de m al cuadro, 3: La sumatoria de los inversos, seleccione: "))

if(opc == 1 ):
    n = int(input("Ingrese el número hasta donde requiere llevar el factorial  "))
    nf=1
    for i in range(1,n+1):
        nf = nf * i
    print(f"El factorial de {n} es {nf}")
if(opc == 2):
    n = int(input("Ingrese el número hasta donde requiere llevar la sumatoria "))
    nf = 0
    for i in range(1,n+1):
        nf = nf + i**2
        print(i)
    print(f"La sumatoria de {n} al cuadrado es {nf}")
if(opc == 3):
    n = int(input("Ingrese el número hasta donde requiere llevar la sumatoria de inversos "))
    nf = 0
    for i in range(1,n+1):
        nf = nf + (1/i)
    print(f"La sumatoria de {n} al cuadrado es {nf}")