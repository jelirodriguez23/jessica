## Elabore un programa en python que lea varios n´´umeros y calcule la suma de las raices cuadradas de los números leídos .
# La lectura termina cuando el npuemro ingresado sea cero
# hacer uso de las isntrucciones TRY-Execption para controlar los posibles errores de la lectura

from math import *
bandera = False
valor = True
t=0
while not bandera:
    try:
        while (valor):
            x=int(input("Ingrese el número "))
            x= sqrt(x)
            t = t+x
            if (x == 0):
                valor = False
                bandera = True
                break
    except IOError:
        print("No es un número")
    except ValueError:
        print("Value error: No es un número o el valor es negativo. ")
    except ZeroDivisionError:
        print("Ha ocurrido un error en la divicion, por cero")
    except :
        print("Error desconocido")
    else:
        print("La suma de las raices cuadradas son: ",t)

