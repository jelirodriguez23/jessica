from tkinter import *
import tkinter as tk
from tkinter import messagebox as m 
from tkinter import font as f
from typing import Sized



raiz = Tk()  # Así creo una ventana
raiz.title("Ventana de Jessica") # 
raiz.resizable(1,1) #primero ancho y luego alto si es cero , cero no me deja cambiar el tamñao de la ventana
raiz.iconbitmap("discor.ico")
raiz.geometry("100x700")

def func():
    m.messagebox.showinfo( "Hello Jessica", "Hiciste click al botón")

def minimizar():
    raiz.iconify()


##RECETA PARA CREAR MENUS
#Paso1. Crear la barra de menús
# font="family='Courier', slant = 'italic', size = 10"
barramenu =Menu(raiz)
#Paso2. Crear la barra de menús
menuArc=Menu(barramenu , tearoff = 0)
menuEdi=Menu(barramenu , tearoff = 0)
#Paso3. Crear la barra de menús
#Crear los comandos de los menus
menuArc.add_command(label="Abrir")
menuArc.add_command(label="Nuevo")
menuArc.add_command(label="Guardar")
menuArc.add_command(label="Cerrar")
menuArc.add_separator()
menuArc.add_command(label="Salir")
menuEdi.add_command(label="Abrir")
#paso4. Agregar los menus a la barra de menús
#menuArc.add_checkbutton(label="hola")
barramenu.add_cascade(label="Archivo",menu=menuArc)
barramenu.add_cascade(label="Edición",menu=menuEdi)
#paso5. Indicamos que la barra de menus estará en la ventana 
raiz.config(bg="#F6CED8",menu=barramenu)

#OTRO

bfont = f.Font(family='Lucida Console', weight = 'bold', size = 20)
b1 = Button(raiz, text='Abriste otra ventana... ', background='#F7819F', fg='#000000', command = func)
b1['font'] = bfont
b1.pack()

menuArc.add_separator()
payment_method_label=Label(raiz, text="Seleccione el método:")
payment_method = StringVar()
payment_method.set("card")


texto = Label(raiz, text="Seleccione el método:" ).place(x=5, y=60, width=200, height=30)
cards = Radiobutton(raiz, text="Debit/Credit Card",variable=payment_method,value="card").place(x=5, y=100, width=200, height=30)
wallet = Radiobutton(raiz, text="Payment Wallet",variable=payment_method,value="wallet").place(x=5, y=140, width=200, height=30)
#netbanking = Radiobutton(raiz, text="Net Banking", variable=payment_method, value="net banking").pack(anchor=tk.W)

raiz.label = Label(raiz, text="...desde Tkinter!") #.place(x=5, y=200, width=200, height=30)
raiz.label.pack()

raiz.entry = Entry(raiz)
raiz.entry.pack(side=tk.LEFT)
#.place(x=5, y=260, width=200, height=30, Sized=LEFT)

# etiqueta = Label( raiz, text="Etiqueta")
# etiqueta.grid(row=400, column=400)

etiqueta2 = Label(raiz, text="Etiqueta2")
etiqueta2.place(x=300, y=300)

button1 = Button(raiz, text="Boton", command=minimizar )
#button1.grid(row=800, column=800)


raiz.geometry("650x370") # ("600x600+0+0")
raiz.mainloop() # instruccion siempre al final


#Eventos .. acciones que hace el usuario, que se llaman interrupciones 250 acciones/milisegundo
# mouse, drag in , drog in etc