print("Ingrese el valor de la función que quisieras realizar.")
print("1, Area y superficie plana")
print("2, Area y volúmen de cuerpos geométricos.")
print("----------------------------------------------------------")

x= int(input("Ingrese el valor: "))
area,vol = 0,0

if x == 1:
    a = int(input("Ingrese el area a realizar 1: Cuadrado \n 2: Rectangulo \n 3: Circulo \n 4: Triangulo "))
    if a==1:
        l = float(input("Ingrese el valor del lado del cuadrado "))
        area = l*l
        print(f"El valor del area es de {area}")
        print("")
    elif a==2:
        b = float(input("Ingrese el valor de la base del rectangulo "))
        c = float(input("Ingrese el valor de la altura "))
        area = b*c
        print(f"El valor del area es de {area}")
        print("")
    elif a==3:
        r = float(input("Ingrese el valor del radio del circulo "))
        pi= 3,1416
        area = pi*r**2
        print(f"El valor del area es de {area}")
        print("")
    elif a==4:
        b = float(input("Ingrese el valor de la base del triangulo "))
        c = float(input("Ingrese el valor de la altura "))
        area = b*c/2
        print(f"El valor del area es de {area}")
        print("")
    else:
        print("El valor ingresado no es valido..")
else:
    
    a = int(input("Cubo o hexaedro : ingresa el valor de un lado "))
    area = 6*a**2
    vol = 6*a**3
    print(f"El area del cubo es {area} y el volumen sería {vol}")
    print("")

    a = int(input("Paralelopipedo : ingresa el valor de un lado de a,b,c "))
    area = 6*a**2
    vol = 6*a**3
    print(f"El area del cubo es {area} y el volumen sería {vol}")