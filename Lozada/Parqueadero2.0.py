from tkinter import ttk
from tkinter import *
from tkinter import messagebox
import sqlite3
import time
import datetime


class Parking:
    DB="D:\Parqueadero.db"
    def __init__(self, raiz):
        #inicio
        self.vent=raiz
        self.vent.title('Parqueadero')
        self.vent.config(bg="gray") #Editar
        self.vent.resizable(0,0)
        frame=LabelFrame(self.vent, text='Ingrese datos')
        frame.grid(row=0,column=0,columnspan = 3,pady=20)
        Label(frame, text="Placa: ", font="Arial").grid(row=0, column = 0, sticky="e")
        self.placa=Entry(frame)
        self.placa.grid(row=0, column=1)
        Label(frame, text="Tipo de vehiculo: ", font="Arial").grid(row=1,column=0, sticky="e")
        self.veh=ttk.Combobox(frame, width=17,state="readonly")
        self.veh.grid(row=1, column=1)
        self.veh["values"]=["Carro","Moto","Bicicleta"]
        Label(frame, text="Hora: ", font="Arial").grid(row=2,column=0, sticky="e")
        self.hora=Label(frame)
        self.hora.grid(row=2,column=1)
        self.tiempo()
        Button(frame, text="Entrada", bg="green", font="arial",command=self.entrada).grid(row=4,column=0)
        Button(frame, text="Salida", bg="red", fg="white", font="arial",command=self.salir).grid(row=4,column=1)

        ### USUARIO ###
        self.usu=Toplevel(raiz, bg="gray")
        self.usu.focus_set()
        self.usu.grab_set()
        self.usu.transient(master=raiz)
        self.usu.title("Usuario")
        self.usu.resizable(0,0)
        self.usu.geometry("250x100")
        Label(self.usu, text="Usuario:",bg="gray", font="Arial").grid(row=1,column=1, sticky="e")
        self.cuadrotexto=Entry(self.usu)
        self.cuadrotexto.grid(row=1, column=2)
        Label(self.usu, text="Contraseña:",bg="gray", font="Arial").grid(row=2,column=1)
        self.cuadrotexto2=Entry(self.usu)
        self.cuadrotexto2.grid(row=2, column=2)
        self.cuadrotexto2.config(show='*')
        boton=Button(self.usu, text="Aceptar", bg="green", font="arial",command=self.usuarios)
        boton.place(x=90,y=60)
        # usu.protocol("WM_DELETE_WINDOW", cerrar)
        self.usu.mainloop()

    def tiempo(self):
	    current_time=time.strftime("%H:%M:%S") 
	    self.hora.config(text=current_time,font="Arial")
	    self.hora.after(200,self.tiempo)

    def run_query(self, accion, parametros=()):
        with sqlite3.connect(self.DB) as miconexion:
            cursor=miconexion.cursor()
            resultado=cursor.execute(accion, parametros)
            miconexion.commit()
        return resultado
        
    def usuarios(self):
        miconexion=sqlite3.connect("D:\Parqueadero.db")
        micursor=miconexion.cursor()    
        micursor.execute("select * from Usuario")
        usuario=micursor.fetchone()
        c=self.cuadrotexto.get()
        cc=self.cuadrotexto2.get()
        if usuario[1]==c and usuario[2]==cc:
            self.usu.destroy()
            return True
        elif usuario[1]!=c:
            messagebox.showinfo(title="Usuario", message="Usuario Incorrecto")
        else:
            messagebox.showinfo(title="Usuario", message="Contraseña Incorrecta")
            self.usu.mainloop()
        miconexion.commit()
        miconexion.close()

    def entrada(self):
        self.modal=Toplevel(raiz, bg="gray")
        self.modal.title('Seleccione el lugar')
        self.modal.transient(master=raiz)
        # accion='INSERT INTO Pedido VALUES(NULL, NULL, ?, ?, ?, NULL, NULL)'
        # parametros=(self.placa.get(), self.veh.get(), datetime.datetime.now())
        # self.run_query(accion, parametros)
        # hora_total=hora_entrada-datetime.timedelta(hora_entrada)
        self.placa.delete(0, END)
        self.modal.mainloop()

    def salir(self):
        accion='SELECT * FROM Pedido'
        h=self.run_query(accion)
        comp=h.fetchone()
        placa=self.placa.get()
        for i in range(len(comp)):
            if placa==comp[2]:
                # tiempo_total=datetime.datetime.now()-comp[4]
                # accion='INSERT INTO Pedido VALUES(NULL, NULL, NULL, NULL, NULL, ?, ?)'
                # parametros=(datetime.datetime.now(), tiempo_total)
                # self.run_query(accion, parametros)
                self.modal=Toplevel(raiz, bg="gray")
                self.modal.title('Factura')
                self.modal.transient(master=raiz)
                self.modal.mainloop()
        # else:
        #     messagebox.showinfo(title="Salida", message="El vehiculo no se encuentra en el parqueadero")

# "SELECT Contra FROM DATOS_USUARIO WHERE Usuario='"+User+"';"

if __name__ == '__main__':
    raiz=Tk()
    aplicacion=Parking(raiz)
    raiz.mainloop()