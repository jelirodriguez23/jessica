import sqlite3
from tkinter import * 
import time
from tkinter.ttk import Combobox
from tkinter import messagebox

def cerrar():
    if messagebox.askokcancel("Salir", "¿Deseas salir?"):
        usu.destroy()
        raiz.destroy()
def usuarios():
    micursor.execute("select * from Usuario")
    usuario=micursor.fetchone()
    c=cuadrotexto.get()
    cc=cuadrotexto2.get()
    if usuario[1]==c and usuario[2]==cc:
        usu.destroy()
        return True
    elif usuario[1]!=c:
        from tkinter import messagebox
        messagebox.showinfo(title="Usuario", message="Usuario Incorrecto")
    else:
        from tkinter import messagebox
        messagebox.showinfo(title="Usuario", message="Contraseña Incorrecta")   
def tiempo():
	current_time=time.strftime("%H:%M:%S") 
	hora.config(text=current_time,bg="gray",font="Arial")
	hora.after(200,tiempo)
def entrada():
    modal=Toplevel(raiz, bg="gray")
    modal.title('Seleccione el lugar')
    modal.transient(master=raiz)
    # modal.geometry("500x500")
    imagen_c=PhotoImage(file="D:\Carro.png")
    imagen_m=PhotoImage(file="D:\Moto.png")
    imagen1=imagen_c.subsample(15)
    imagen2=imagen_m.subsample(15)
    # Label(modal, image=imagen).pack()
    
    # Label(modal, text="1",bg="gray").grid(row=0,column=0)
    # Checkbutton(modal, bg="gray").grid(row=1,column=0)
    # Label(modal, text="2",bg="gray").grid(row=0,column=1)
    # Checkbutton(modal, bg="gray").grid(row=1,column=1)
    # Label(modal, text="3",bg="gray").grid(row=0,column=2)
    # Checkbutton(modal, bg="gray").grid(row=1,column=2)
    # Label(modal, text="4",bg="gray").grid(row=3,column=0)
    # Checkbutton(modal, bg="gray").grid(row=2,column=0)
    # Label(modal, text="5",bg="gray").grid(row=3,column=1)
    # Checkbutton(modal, bg="gray").grid(row=2,column=1)
    # Label(modal, text="6",bg="gray").grid(row=3,column=2)
    # Checkbutton(modal, bg="gray").grid(row=2,column=2)
    Button(modal, image=imagen1).grid(row=0, column=0)
    Button(modal, image=imagen1).grid(row=0, column=1)
    Button(modal, image=imagen1).grid(row=0, column=2)
    Button(modal, image=imagen1).grid(row=0, column=4)
    Button(modal, image=imagen1).grid(row=0, column=5)
    Label(modal, text="===ENTRY===>", fg="yellow", bg="black").grid(row=1, column=0)
    Button(modal, image=imagen2).grid(row=2, column=0)
    Button(modal, image=imagen2).grid(row=2, column=1)
    Button(modal, image=imagen2).grid(row=2, column=2)
    Button(modal, image=imagen2).grid(row=2, column=4)
    Button(modal, image=imagen2).grid(row=2, column=5)
    # boton.grid(row=4, column=1)
    modal.mainloop()
def salir():
    from tkinter.messagebox import showinfo
    placa1=placa.get()
    micursor.execute("insert into Pedido values ('1','0','"+str(placa1)+"','0','1','1','1')")
    # hora1=tiempo.()
    # showinfo(message="Placa "+str(placa1)+"\n Hora "+str(hora1))

miconexion=sqlite3.connect("D:\Parqueadero.db")
micursor=miconexion.cursor()

#PROGRAMA PRINCIPAL#
raiz=Tk()
raiz.title("Parqueadero") #Titulo
raiz.resizable(0,0) #Impedir 0/ permitir 1 redimension
raiz.geometry("500x300")
raiz.config(background="gray")
Label(raiz, text="Placa:",bg="gray", font="Arial").grid(row=0,column=0, sticky="e")
placa=Entry(raiz)
placa.grid(row=0, column=1)
h=placa.get()
Label(raiz, text="Tipo de vehiculo: ",bg="gray", font="Arial").grid(row=1,column=0)
t_veh=Combobox(raiz, width=18,state="readonly")
t_veh.grid(row=1, column=1)
t_veh["values"]=["carro","moto"]
Label(raiz, text="Hora: ",bg="gray", font="Arial").grid(row=2,column=0, sticky="e")
hora=Label(raiz)
hora.grid(row=2,column=1)
tiempo()
boton=Button(raiz, text="Entrada", bg="green", font="arial", command=entrada)
boton.grid(row=4,column=0)
boton=Button(raiz, text="Salida", bg="red", fg="white", font="arial", command=salir)
boton.grid(row=4,column=1)
usu=Toplevel(raiz, bg="gray")
usu.focus_set()
usu.grab_set()
usu.transient(master=raiz)
usu.title("Usuario") #Titulo
usu.resizable(0,0) #Impedir 0/ permitir 1 redimension
usu.geometry("250x100")
nomlabel=Label(usu, text="Usuario:",bg="gray", font="Arial").grid(row=1,column=1, sticky="e")
cuadrotexto=Entry(usu)
cuadrotexto.grid(row=1, column=2)
nomlabel2=Label(usu, text="Contraseña:",bg="gray", font="Arial").grid(row=2,column=1)
cuadrotexto2=Entry(usu)
cuadrotexto2.grid(row=2, column=2)
boton=Button(usu, text="Aceptar", bg="green", font="arial",command=usuarios)
boton.place(x=90,y=60)
# usu.protocol("WM_DELETE_WINDOW", cerrar)
usu.mainloop()
raiz.mainloop()
miconexion.commit()
miconexion.close()
# #########
# miConexion=sqlite3.connect("D:\Parqueadero.db")
# miCursor=miConexion.cursor()
# a=input("Digite hora de salida ")
# tr=str("insert into Pedido values ('1 pm','"+a+"','xvy123','2','carro','1','121')")
# miCursor.execute(tr)
# miConexion.commit()
# miConexion.close()