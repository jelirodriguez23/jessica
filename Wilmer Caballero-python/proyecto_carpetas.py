import os
import time

os.system ("cls")

def archivooos(cp1,cp2):
    with os.scandir(cp1) as archivos:
        print("-"*(85))
        print("| CARPETA NUMERO 1:",os.path.basename(cp1),""*(4)," | Tamaño:",sum(cp1.stat().st_size for cp1 in os.scandir('.') if cp1.is_file()))
        print("| Catidad de archivos: ",len(os.listdir(cp1)))
        print("| Creado:",time.ctime(os.path.getctime(cp1)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp1)))
        print("-"*(85))
        for archivo in archivos:
            d=(os.path.getsize(archivo)/1000)
            print("|",archivo.name.ljust(50),f"|  {d:.1f} KB")
        print("-"*(85))
        print("/"*(85))

    with os.scandir(cp2) as archivos2:
        print("-"*(85))
        print("| CARPETA NUMERO 2:",os.path.basename(cp2),""*(4)," | Tamaño:",os.path.getsize(cp2))
        print("| Catidad de archivos: ",len(os.listdir(cp2)))
        print("| Creado:",time.ctime(os.path.getctime(cp2)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp2)))
        print("-"*(85))
        for archivo in archivos2:
            d=(os.path.getsize(archivo)/1000)
            print('|',archivo.name.ljust(50),f"|  {d:.1f} KB")
        print("-"*(85))

def comparar_archivos (cp1,cp2):
    cont = 0
    with os.scandir(cp1) as archivos:
        for archivo1 in archivos:
            with os.scandir(cp2) as archivos2:
                for archivo2 in archivos2:
                    if (archivo1.name == archivo2.name):
                        cont +=1
                        print("La archivo repetido es:",archivo1.name)
    print("Achivos repetidos: ", cont)

def comparar_tamaño(cp1,cp2):
    cont = 0
    with os.scandir(cp1) as archivos:
        for archivo1 in archivos:
            with os.scandir(cp2) as archivos2:
                for archivo2 in archivos2:
                    tar1= os.path.getsize(archivo1)/1000
                    tar2= os.path.getsize(archivo2)/1000
                    if (tar1 == tar2):
                        cont +=1
                        print("El archivo con el mismo tamaño es:",archivo1.name.ljust(50),"| Tamaño",f"{tar1:.1f} KB")
    print("Achivos con el mismo tamaño: ", cont)


#d:\Users\user\Desktop\Curso phyton
#d:\Users\user\Desktop\Curso phyton\python
continuar = True
while continuar == True:    
    Carpeta1 = str(input('Ingrese la ruta de la carpeta 1: '))
    cp1 = Carpeta1.replace(" \ "," / ")
    if (os.path.exists(cp1) == False): 
        print(" ///////////// Direccion Erronea /////////////")
    else:
        continuar2 = True
        while continuar2 == True:
            carpeta2 = str(input('Ingrese la ruta de la carpeta 2: '))
            cp2 = carpeta2.replace(" \ "," / ")
            if(os.path.exists(cp2) == False ):
                print(" ///////////// Direccion Erronea /////////////")
            else:
                continuar2 =  False
                continuar  =  False
    
    if(cp1 == cp2):
        c = input("SON LA MISMA CARPETA, DESEA CONTINUAR? (S/N): ")
        if (c.upper() == 'S'):
            continuar2 =  False
            continuar  =  False
            break
        else:
            continuar2 =  True
            continuar  =  True
            os.system("cls")


menu = {1:archivooos,2:comparar_archivos,3:comparar_tamaño}
seguir = True

while( seguir ==  True):
    with os.scandir(cp1) as archivos:
        print("\n INFORMACION GENERAL:")
        print("-"*(90))
        print("| CARPETA NUMERO 1:",os.path.basename(cp1),""*(4)," | Tamaño:",sum(cp1.stat().st_size for cp1 in os.scandir('.') if cp1.is_file()))
        print("| Catidad de archivos: ",len(os.listdir(cp1)))
        print("| Creado:",time.ctime(os.path.getctime(cp1)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp1)))
        print("-"*(90))

        
    with os.scandir(cp2) as archivos2:
        print("-"*(90))
        print("| CARPETA NUMERO 2:",os.path.basename(cp2),""*(4)," | Tamaño:",os.path.getsize(cp2))
        print("| Catidad de archivos: ",len(os.listdir(cp2)))
        print("| Creado:",time.ctime(os.path.getctime(cp2)),"| Ultima modificacion:",time.ctime(os.path.getmtime(cp2)))
        print("-"*(90))

    print("\t MENU PRINCIPAL \n"+"1.Mostrar Archivos \n"+"2.Buscar Archivos repetidos\n"+"3.Comparar archivos segun su tamaño\n"+"4.Finalizar")      
    try:
        opcion = int(input("\n Digite la operacion deseada: "))
        print()
    except ValueError:
        print("/////// Dato no valido /////////")
    else:
        if opcion != 4: 
            menu[opcion](cp1,cp2)
        else:
            print("PROGRAMA FINALIZADO...")
            seguir = False

    input("Presone para volver")
    os.system("cls")
