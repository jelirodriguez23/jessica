from django.db import models

# Create your models here.

class Empleado(models.Model):
    id= models.CharFiel(max_length=50, primary_key= True)
    nombre= models.CharField(max_length=50, null=True)
    apellido = models.CharField(max_length=50, null=True)
    contraseña = models.CharField(max_length=50, null= True)
    telefono = models.CharField(max_length=50, null=True)

class Facutura(models.Model):
    ID_Factura=models.CharFiel(max_length=50, primary_key= True)
    Valor= models.CharField(max_length=50, null=True)
    Fecha = models.CharField(max_length=50, null=True)
    Hora = models.CharField(max_length=50, null=True)
    Descripcion_Servicio = models.CharField(max_length=50, null=True)
    Cod_Reserva = models.CharField(max_length=50, foreign_key=True)

class Habitacion(models.Model):
    Numero_Habitacion = models.CharField(max_length=50, primary_key= True)
    Cupo_maximo= models.CharFiel(max_length=50, null = True)
    Tipo_Habitacion= models.CharField(max_length=50, null=True)
    Estado_Habitacion = models.CharField(max_length=50, null=True)

class Huesped(models.Model):
    ID_Husped = models.CharField(max_length=50, primary_key= True)
    Nombre= models.CharFiel(max_length=50, null= True)
    Apellido= models.CharField(max_length=50, null=True)
    Correo = models.CharField(max_length=50, null=True)
    Telefono = models.CharField(max_length=50, null = True)
    Tipo_Tarjeta = models.CharField(max_length=50, null = True)
    Numero_Tarjeta = models.CharField(max_length=50, null = True)
    Fecha_Expedicion  = models.CharField(max_length=50, null = True)

class Reserva(models.Model):
    Cod_Reserva = models.CharField(max_length=50, primary_key= True)
    Fecha_Ingreso = models.CharFiel(max_length=50, primary_key= True)
    Fecha_Salida = models.CharField(max_length=50, null=True)
    Cantidad_Niños = models.CharField(max_length=50, null=True)
    Cantidad_Adultos = models.CharField(max_length=50, null=True)
    Estado_Reserva = models.CharField(max_length=50, null=True)
    Numero_Habitacion = models.CharField(max_length= 50, foreing_key = True)
    ID_Huesped = models.CharField(max_length= 50, foreing_key = True)
    ID_Empleado = models.CharField(max_length= 50, foreing_key = True)