import math
print("PROGRAMA CAJERO")
c=int(input("Oprima cero si desea finalizar el programa o cualquier otro número si desea continuar "))
while(c!=0): 
    n=int(input("Ingrese el valor que desea retirar, recuerde que debe ser múltiplo de 10000 y como máximo puede retirar un millón $"))
    while(n%10000!=0 or n>=1000000):
        n=int(input("Valor incorrecto, recuerde que debe ser múltiplo de 10000 y como máximo puede retirar un millón $"))
    a=n/50000
    if(a>=1):
     a=math.floor(a)
    b=n-50000*a
    c=b/20000
    if(c>=1):
     c=math.floor(c)
    d=b-20000*c
    e=d/10000
    e=math.floor(e)
    print("El cajero le da al usuario la suma de $",n,"en",a,"billetes de $50000,",c,"billetes de $20000 y",e,"billetes de $10000.")
    c=int(input("Oprima cero si desea finalizar el programa o cualquier otro número si desea realizar otro retiro "))
print("FIN DEL PROGRAMA")
