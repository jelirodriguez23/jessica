import math
print("PROGRAMA PARA GENERAR MATRIZ MÁGICA")
z=int(input("Si desea finalizar el programa ingrese 0, de lo contrario ingrese otro número entero para inicia el programa "))
while(z!=0):
    M=[[]]
    n=2
    while(n%2==0 or n<3):
        try:
          n=int(input("Ingrese un número entero impar mayor o igual que 3 para definir la dimensión de la matriz mágica "))
          if(n%2==0 or n<3):
              print("¿Acaso usted no finalizó la primaria? Recuerde que debe ingresar un número ENTERO IMPAR MAYOR O IGUAL QUE 3")
        except ValueError:
            print("¿Porqué ingresa una letra? ¡Vamos que no es tan difícil! Un NÚMERO ENTERO IMPAR MAYOR O IGUAL QUE 3")
    for fil in range(n):
        M.append([])
        for col in range(n):
            M[fil].append(0)
    a=1
    i=0
    c=(n//2)
    j=c
    p =n**2
    while(a<=(p)):
        if(M[i][j]==0):
            M[i][j]=a
        else:
            i=i+1
            j=j 
            M[i][j]=a
        a=a+1
        j=j+1
        i=i-1
        if(i<0 and j==n):
            i=n-1
            j=0
        if (i<0):
            i=i+n
        if(j==n):
            j=0
    print("----------------------------------------------------------------------------------------")
    print("La matriz mágica",n,"x",n,"es:")
    for fil in M:
        print(fil)
    w=(((n**2)//2)+1)
    q=w*n
    print("La suma de cualquier diagonal, columna o fila es igual a",q)
    print("La matriz con la suma de sus filas incluidas es:")
    filas=len(M)
    for i in range(filas):
        M[i].append(q)
    for fil in M:
        print(fil)
    z=int(input("Si desea finalizar el programa ingrese 0 de lo contrario ingrese otro número entero para generar otra matriz mágica "))
print("FIN DEL PROGRAMA")