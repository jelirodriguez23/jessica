import random
matriz=[[]]
m=int(input("Ingrese el número de filas y columnas "))
for fila in range(m):
    matriz.append([])
    for col in range(m):
        matriz[fila].append(random.randint(10,1000))
print("La matriz original es: ")
for fila in matriz:
    print(fila)
print("La primera fila es: ")
print(matriz[0])
print("La última fila es: ")
print(matriz[m-1])
print("La primera columna es:")
c=[]
for i in range(m):
    d=matriz[i][0]
    c.append(d)
print(c)
print("La última columna es:")
e=[]
for i in range(m):
    f=matriz[i][m-1]
    e.append(f)
print(e)
print("La diagonal principal es:")
g=[]
for i in range (m):
    h=matriz[i][i]
    g.append(h)
print(g)
print("La diagonal secundaria es:")
j=[]
for i in range(m-1,-1,-1):
    for l in range(m):
       if(i+l==m-1):
         k=matriz[l][i]
         j.append(k)
print(j)



