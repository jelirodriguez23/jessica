import os
print("PROGRAMA PARA DETERMINAR LA CANTIDAD DE REBOTES QUE DA UNA PELOTA ANTES DE LLEGAR A UNA ALTURA MENOR A O.5 METROS")
h=float(input("Ingrese la altura desde la que cae la pelota en metros "))
r=0
k=float(input("Ingrese el valor del coeficiente k "))
while(h>=0.5):
   h=h-h*k
   r=r+1
H=round(h,3)
print("La altura final de la pelota es",H,"metros")
print("La cantidad de rebotes de la pelota fue",r)
