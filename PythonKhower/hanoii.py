def hanoi (n,ori,des,tem):
    global movimientos
    if n == 1:
        movimientos +=1
        print(movimientos, ori,"---->",des)
    else:
        hanoi (n-1, ori, tem, des)
        hanoi (1, ori, des, tem)
        hanoi (n-1,tem,des,ori)
movimientos = 0
discos=int(input("Digite el número de discos: "))
hanoi(discos, "poste 1","poste 2","poste 3")