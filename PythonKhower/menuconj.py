# Dados los siguientes conjuntos:
# A = {1,2,3,4,5,6,7,8,9,10}
# B = {1,2,3,5,8,13,21,34,55}
# C = {2,4,6,8,10,12,14,16,18}

# Elabore un programa Python que, a través del menú de opciones, realice y muestre las siguientes operaciones:
# MENÚ PRINCIPAL
# 1.(AUB)&C
# 2.(A&B)-C
# 3.A-(BUC)
# 4. Salir

# Use definición de funciones para cada opción de menú.
# Use un diccionario para gestionar las opciones de menu.

# Ejemplo:
# MENÚ PRINCIPAL
# 1.(AUB)&C = {2,4,6,8,10}
# 2.(A&B)-C = {1,3,5}
# 3.A-(BUC) = {7,9}
# 4. Salir

def op1(A,B,C):
    return((A|B)&C)
def op2(A,B,C):
    return((A&B)-C)
def op3(A,B,C):
    return(A-(B|C))

A = {1,2,3,4,5,6,7,8,9,10}
B = {1,2,3,5,8,13,21,34,55}
C = {2,4,6,8,10,12,14,16,18}

iniciar = True
while (iniciar):

    print("Conjunto A",A)
    print("Conjunto B",B)
    print("Conjunto C",C)
    print("MENÚ PRINCIPAL")
    print ("1. Opcion 1:(AUB)&C")
    print ("2. Opcion 2: (A&B)-C")
    print ("3. Opcion 3: A-(BUC)")
    print ("4. Salir")
    n = int(input("Elija una opción digitando el número correspondiente en el teclado: "))  
    opciones={1:op1,2:op2,3:op3}
    resultado=opciones[n](A,B,C)
    print(resultado)
print ("Fin")