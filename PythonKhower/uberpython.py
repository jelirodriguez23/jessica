""" 1) Leer el valor aportado por el conductor en la plataforma InDriver
2) Leer el valor de las diversas careras que el conductor realiza efectivamente y descontar el 10% de cada una.
3) Realizar el punto anterior tantas veces como sea posible hasta agotar o sobrepasar (una sola vez) el monto aportado
por el conductor a la plataforma
4) Reportar el número total de carreras, el valor acumulado por todas las carreras y el valor aportado a la plataforma """

valorplat = 0
valorcarrera = 0
descuento = 0
contarcarrera= 0
valtotalcarrera = 0
aporteplat = 0

valorplat = int(input("Ingrese por favor el valor aportado para la plataforma InDriver: "))
valorcarrera = int(input("Ingrese el valor de la carrera: "))
while (valorplat >= 0):
    descuento = valorcarrera * 0.1
    contarcarrera = contarcarrera + 1
    valortotalcarrera = aporteplat * contarcarrera
    aporteplat = valorcarrera - descuento
    if (valorcarrera > valorplat):
        print("Sólo puede realizar una carrera más, debe volver a recargar")
print("El valor total de la carrera es", descuento)