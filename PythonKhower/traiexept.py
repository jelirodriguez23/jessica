# LecturaValida = False
# while not LecturaValida:
#     try:
#         x=int(input("Digite un número: "))
#         LecturaValida = True
#     except:
#         print("Ha ocurrido un error.")
# print("El programa sigue su curso.")

# try:
#     x=int(input("Ingrese un valor: "))
# except Exception as miError:
#     print("Ocurrió este error --> ", miError)
# else:
#     print("No ocurrió error alguno")

#Manejo de Múltiples excepciones

try:
    x=int(input("Deme un valor: "))
    y=int(input("Segundo Valor: "))
    z=x/y
except IOError: #InputOutput error, ej: el archivo no existe.
    print("No se pudo leer.")
except ZeroDivisionError:
    print("No se puede dividir por cero.")
except ValueError:
    print("Dato no válido.")
except:
    print("Error desconocido.")
else:
    print("No hay ningún error, puede continuar :3")