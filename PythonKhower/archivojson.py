import json

datos = {}
datos['clientes'] = []

datos['clientes'].append({
    'primer_nombre':'Katerin',
    'apellido':'Hower',
    'edad':24,
    'ventas':1.11})
datos['clientes'].append({
    'primer_nombre':'Kiki',
    'apellido':'Astronomica',
    'edad':21,
    'ventas':7.13})
with open('datos.json', 'w') as file:
    json.dump(datos,file,indent=4)