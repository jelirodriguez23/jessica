def leermes ():
    mesvalido = False
    while not(mesvalido):
        mes =int(input("Ingrese un número del 1 al 12 por favor: "))
        if(mes>=0) and (mes<=12):
            mesvalido=True
        else:
            print("El número digitado no corresponde a un número válido, por favor, ingrese un número del 1 al 12.")
    return (mes)
mesesaño = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
m = leermes()
while m != 0:
    print (mesesaño[m-1])
    m = leermes()
print("Fin Programa")