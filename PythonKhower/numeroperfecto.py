""" Un número perfecto es un número natual que es igual a la suma de sus divisores propios. """
acu = 0
n = int(input("Por favor, ingrese un número: "))

for i in range (1, n):
    if n % i == 0:
        acu = acu + i

if (acu == n):
    print("Es un número perfecto")
else:
    print("No es un número perfecto")