# Elabore un programa python que lea varios números y calcule la suma de las raíces cuadradas de los números leídos.
# La lectura termina cuando el número ingresado sea CERO.
# Hacer uso de las instrucciones Try-Except para controlar los posibles errores de lectura y cálculo
import os
def raizc (x):
    return (x**0.5)
calcular = True
sumarz = 0
while calcular:
    try:
        n=int(input("Ingrese un número: "))
    except ValueError:
        print("El Dato no es válido.")
    except ZeroDivisionError:
        print("No se puede dividir por cero.")
    except:
        print("Error desconocido")
    else:
        if n != 0:
            # print(raizc(n))
            sumarz = sumarz + raizc(n)
            print(sumarz)
        else:
            print("Fin del programa")
            calcular = False
print("Fin")