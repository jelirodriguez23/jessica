#No se pueden ingresar nombres repetidos
agenda = {}
def nombreunico (nom,agenda):
    if nom in agenda:
        return False
    else:
        return True
def telefonounico (tel,agenda):
    if tel in agenda:
        return False
    else:
        return True
seguir = True
while seguir:
    nombre = input("Ingrese el nombre por favor: ")
    telefono = int(input("Ingrese el número de teléfono por favor: "))
    if nombreunico(nombre,agenda):
        if telefonounico(telefono,agenda):
            agenda[nombre]=telefono
    else:
        print("El nombre no se puede agregar, ya está en la agenda")
    r = input("¿Quiere agregar más contactos? Digite 1 para Sí ")
    seguir = (r == "1")
for (a,b) in agenda.items():
    print(f"La persona {a} ha sido agregada a la agenda.")
    print(a.ljust(20),b)